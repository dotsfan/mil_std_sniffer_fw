#include "main.h"

// Используемый сокет: 192.168.1.15:4000 (файл udp.c)
// Физический адрес: 02:34:56:78:9A:BC (файл ethfunc.c)

int main()
{
    // Настройка тактирования МК. Частота 96 МГц
    RST_CLK_Config();
    
    // Настройка MIL_STD_1553
    MIL_STD_PortConfig();
    MIL_STD_Config();
    MIL_STD_StructInit();
    
    // Настройка Ethernet, АРМ
    UDP_Init();
    Eth_PortInit();
    ARM_StructInit();
    ETHERNET_Config();
    
    LEDS_PortInit();
    
    while(1)
    {
        Eth_Link();
        
        ARM_DetectPacket();
        ARM_ParsePacketRx();
        ARM_ParsePacketTx();
    }
}
