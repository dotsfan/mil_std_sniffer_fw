/**
	*****************************************************************************
	* @file			...\src\MIL_STD\mil_std_app.c
	* @date			09-04-19
	* @brief		Содержит реализацию модуля взаимодействия с интерфейсом MIL_STD_1553
	*****************************************************************************
	*/

/* Includes -----------------------------------------------------------------*/
#include "MDR1986VE1T.h"
#include "mil_std_app.h"
#include "ring_buffer.h"

///////////////////////////////////////////////////////////////////////////////
typedef enum
{
	CMD_MSG_BS_RT			= 0x0001, // Команда приема КШ-ОУ, НЕ групповая (формат 1)
	CMD_MSG_RT_BS			= 0x0402, // Команда передачи ОУ-КШ, НЕ групповая (формат 2)
	CMD_MSG_RT_RT_TX		= 0x1008, // Команда передачи ОУ-ОУ, НЕ групповая (формат 3)
	CMD_MSG_RT_RT_RX		= 0x0004, // Команда приема ОУ-ОУ, НЕ групповая (формат 3) TODO: Уточнить!
	CMD_MSG_CNTRL			= 0x0410, // Команда управления БЕЗ данных, НЕ групповая (формат 4)
	CMD_MSG_CNTRL_DATA_TX	= 0x2420, // Команда управления с данными (ОУ-КШ), НЕ групповая (формат 5)
	CMD_MSG_CNTRL_DATA_RX	= 0x0040, // Команда управления с данными (КШ-ОУ), НЕ групповая (формат 6)
	CMD_MSG_BS_RT_GROUP		= 0x0080, // Команда приема КШ-ОУ, групповая (формат 7)
	CMD_MSG_RT_RT_TX_GROUP	= 0x0200, // Команда передачи ОУ-ОУ, групповая (формат 8)
	CMD_MSG_RT_RT_RX_GROUP	= 0x0100, // Команда приема ОУ-ОУ, групповая (формат 8) TODO: Уточнить
	CMD_MSG_CNTRL_GROUP		= 0x0400, // Команда управления БЕЗ данных, групповая (формат 9)
	CMD_MSG_CNTRL_DATA_GRP	= 0x0800  // Команда управления с данными, групповая (формат 10)
}msg_reg_t;

///////////////////////////////////////////////////////////////////////////////
typedef struct
{
	__IO uint8_t is_correct_rx:1;		// Признак приема достоверного слова
	__IO uint8_t is_wait_cmd1:1;		// Признак ожидания КС1
	__IO uint8_t is_wait_cmd2:1;		// Признак ожидания КС2
	__IO uint8_t is_wait_reply1:1;		// Признак ожидания ОС1
	__IO uint8_t is_wait_reply2:1;		// Признак ожидания ОС2
	__IO uint8_t is_wait_cntrl_data:1;	// Признак ожидания СД КУ
	__IO uint8_t is_wait_data:1;		// Признак ожидания СД
	__IO uint8_t RESERVED:1;
}mil_std_flags_t;

///////////////////////////////////////////////////////////////////////////////
msg_reg_t msg_reg_ch0;          // Исп. для декодирования принятых сообщений
mil_std_flags_t mil_std_ch0;    // Исп. для управления мониторингом шины
mil_std_flags_t mil_std_status; // Исп. для хранения статусов принятых слов

__IO uint32_t cmd_word1_ch0 = 0;
__IO uint32_t cmd_word2_ch0 = 0;
__IO uint32_t status_word1_ch0 = 0;
__IO uint32_t status_word2_ch0 = 0;
__IO uint32_t mode_data = 0;

__IO uint32_t errors_ch0 = 0;
__IO uint32_t status_ch0 = 0;

__IO uint8_t cnt_of_msg_ch0 = 0;

const uint8_t MIL_STD_CNT_MSG_SIZE_RX = 64;
static uint8_t MIL_STD_MSG_RX[MIL_STD_CNT_MSG_SIZE_RX] __attribute__ ((aligned (4))) = {0};

extern RING_buffer_t udp_ring_tx;
RING_buffer_t mil_std_cnt_word;

///////////////////////////////////////////////////////////////////////////////
void MIL_STD_1553B2_Handler(void);
void MIL_STD_1553B1_Handler(void);

///////////////////////////////////////////////////////////////////////////////
static void MIL_STD_FlagsInit(mil_std_flags_t *mil_std_flags)
{
	mil_std_flags->RESERVED = 0;
	mil_std_flags->is_correct_rx = 0;
	mil_std_flags->is_wait_cmd1 = 1;
	mil_std_flags->is_wait_cmd2 = 0;
	mil_std_flags->is_wait_reply1 = 0;
	mil_std_flags->is_wait_reply2 = 0;
	mil_std_flags->is_wait_cntrl_data = 0;
	mil_std_flags->is_wait_data = 0;
}

///////////////////////////////////////////////////////////////////////////////
static void MIL_STD_StatusInit(mil_std_flags_t *mil_std_flags)
{
	mil_std_flags->RESERVED = 0;
	mil_std_flags->is_correct_rx = 0;
	mil_std_flags->is_wait_cmd1 = 0;
	mil_std_flags->is_wait_cmd2 = 0;
	mil_std_flags->is_wait_reply1 = 0;
	mil_std_flags->is_wait_reply2 = 0;
	mil_std_flags->is_wait_cntrl_data = 0;
	mil_std_flags->is_wait_data = 0;
}

///////////////////////////////////////////////////////////////////////////////
void MIL_STD_StructInit(void)
{
    MIL_STD_FlagsInit(&mil_std_ch0);
    MIL_STD_StatusInit(&mil_std_status);
    RING_Init(&mil_std_cnt_word, MIL_STD_MSG_RX, MIL_STD_CNT_MSG_SIZE_RX);
}

///////////////////////////////////////////////////////////////////////////////
uint8_t MIL_STD_GetCountMSG(void)
{
	return cnt_of_msg_ch0;
}

///////////////////////////////////////////////////////////////////////////////
void MIL_STD_DecrCountMSG(void)
{
	NVIC_DisableIRQ(MIL_STD_1553B2_IRQn);
	cnt_of_msg_ch0--;
	NVIC_EnableIRQ(MIL_STD_1553B2_IRQn);
}

///////////////////////////////////////////////////////////////////////////////
void MIL_STD_SetCountMSG(uint8_t count)
{
	NVIC_DisableIRQ(MIL_STD_1553B2_IRQn);
	cnt_of_msg_ch0 = count;
	NVIC_EnableIRQ(MIL_STD_1553B2_IRQn);
}

// Обработчик 2-ого контроллера основного канала. Режим монитора
void MIL_STD_1553B2_Handler(void)
{
	static uint32_t sub_addr = 0;
	static uint32_t cnt_word = 0;
	static uint32_t data_word = 0;
	static uint32_t index = 0;
	static uint8_t cnt_of_words_ch0 = 0;
	static uint8_t msg_format_ch0 = 0;
    static uint8_t mil_std_flags = 0;
	
	uint32_t status_reg = MDR_MIL_STD_15532->STATUS;
	uint32_t inten_reg = MDR_MIL_STD_15532->INTEN;
	inten_reg &= status_reg;
	
	if(inten_reg & MIL_STD_1553_STATUS_RFLAGN) // Получено достоверное КС, ОС, СД КУ (RFLAGN)
	{
		mil_std_ch0.is_correct_rx = 1;
		mil_std_status.is_correct_rx = 1;
		status_ch0 = status_reg; // Сохраняем статусы для IDLE
		
		if(mil_std_ch0.is_wait_cntrl_data) // Чтение СД КУ
		{
			mil_std_ch0.is_wait_cntrl_data = 0;
			mil_std_status.is_wait_cntrl_data = 1;
			
			// Для сообщений формата 6 устанавливаем признак ожидания ОС1
			if(msg_reg_ch0 == CMD_MSG_CNTRL_DATA_RX)
			{
				mil_std_ch0.is_wait_reply1 = 1;
			}
			// Сохраняем СД КУ
			mode_data = MDR_MIL_STD_15532->ModeData;
			RING_Put16(&udp_ring_tx, (uint16_t)mode_data);
			// Выходим из обработчика, чтобы пропустить чтение ОС1
			return;
		}
		if(mil_std_ch0.is_wait_reply2) // Чтение ОС2
		{
			mil_std_ch0.is_wait_reply2 = 0;
			mil_std_status.is_wait_reply2 = 1;
			
			// Сохраняем ОС2
			status_word2_ch0 = MDR_MIL_STD_15532->StatusWord2;
			RING_Put16(&udp_ring_tx, (uint16_t)status_word2_ch0);
		}
		if(mil_std_ch0.is_wait_reply1) // Чтение ОС1
		{
			mil_std_ch0.is_wait_reply1 = 0;
			mil_std_status.is_wait_reply1 = 1;
			
			// Для сообщений формата 3 устанавливаем признак ожидания ОС2
			if((msg_reg_ch0 == CMD_MSG_RT_RT_TX) || (msg_reg_ch0 == CMD_MSG_RT_RT_RX))
			{
				mil_std_ch0.is_wait_reply2 = 1;
			}
			// Для сообщений формата 5 устанавливаем признак ожидания СД КУ
			else if(msg_reg_ch0 == CMD_MSG_CNTRL_DATA_TX)
			{
				mil_std_ch0.is_wait_cntrl_data = 1;
			}
			// Сохраняем ОС1
			status_word1_ch0 = MDR_MIL_STD_15532->StatusWord1;
			RING_Put16(&udp_ring_tx, (uint16_t)status_word1_ch0);
		}
		if(mil_std_ch0.is_wait_cmd2) // Чтение КС2
		{
			mil_std_ch0.is_wait_cmd2 = 0;
			mil_std_status.is_wait_cmd2 = 1;
			
			// Устанавливаем признаки ожидания ОС1, СД
			mil_std_ch0.is_wait_reply1 = 1;
			mil_std_ch0.is_wait_data = 1;
			// Сохраняем КС2
			cmd_word2_ch0 = MDR_MIL_STD_15532->CommandWord2;
			RING_Put16(&udp_ring_tx, (uint16_t)cmd_word2_ch0);
		}
		if(mil_std_ch0.is_wait_cmd1) // Чтение КС1
		{
			mil_std_ch0.is_wait_cmd1 = 0;
			mil_std_status.is_wait_cmd1 = 1;
			
			cmd_word1_ch0 = MDR_MIL_STD_15532->CommandWord1;
			msg_reg_ch0 = MDR_MIL_STD_15532->MSG;			
			switch(msg_reg_ch0)
			{
				case CMD_MSG_BS_RT: // Формат 1
					msg_format_ch0 = 1;
					// Ожидаем принять СД и ОС1
					mil_std_ch0.is_wait_data = 1;
					mil_std_ch0.is_wait_reply1 = 1;
					break;
					
				case CMD_MSG_RT_BS: // Формат 2
					msg_format_ch0 = 2;
					// Ожидаем принять ОС1 и СД
					mil_std_ch0.is_wait_reply1 = 1;
					mil_std_ch0.is_wait_data = 1;
					break;
					
				case CMD_MSG_RT_RT_TX: // Формат 3
					msg_format_ch0 = 3;
					// Ожидаем принять КС2, ОС1, СД, ОС2
					mil_std_ch0.is_wait_cmd2 = 1;
					// Признаки ожидания ОС1, СД устанавливаются при приеме КС2
					break;
					
				case CMD_MSG_RT_RT_RX: // Формат 3
					msg_format_ch0 = 3;
					// Ожидаем принять КС2, ОС1, СД, ОС2
					mil_std_ch0.is_wait_cmd2 = 1;
					// Признаки ожидания ОС1, СД устанавливаются при приеме КС2
					break;
					
				case CMD_MSG_CNTRL: // Формат 4
					msg_format_ch0 = 4;
					// Ожидаем принять ОС1
					mil_std_ch0.is_wait_reply1 = 1;
					break;
					
				case CMD_MSG_CNTRL_DATA_TX: // Формат 5. TODO: Уточнить кол-во прерываний при приеме ОС1, СД КУ
					msg_format_ch0 = 5;
					// Ожидаем принять ОС1 и СД КУ
					mil_std_ch0.is_wait_reply1 = 1;
					// Признак ожидания СД КУ устанавливаем при приеме ОС1
					break;
					
				case CMD_MSG_CNTRL_DATA_RX: // Формат 6. TODO: Уточнить кол-во прерываний при приеме СД КУ и ОС1
					msg_format_ch0 = 6;
					// Ожидаем принять СД КУ и ОС1
					mil_std_ch0.is_wait_cntrl_data = 1;
					// Признак ожидания ОС1 устанавливается при приеме СД КУ
					break;
					
				case CMD_MSG_BS_RT_GROUP: // Формат 7
					msg_format_ch0 = 7;
					// Ожидаем принять СД
					mil_std_ch0.is_wait_data = 1;
					break;
					
				case CMD_MSG_RT_RT_TX_GROUP: // Формат 8
					msg_format_ch0 = 8;
					// Ожидаем принять КС2, ОС1, СД
					mil_std_ch0.is_wait_cmd2 = 1;
					// Признаки ожидания ОС1, СД устанавливаются при приеме КС2
					break;
					
				case CMD_MSG_RT_RT_RX_GROUP: // Формат 8
					msg_format_ch0 = 8;
					// Ожидаем принять КС2, ОС1, СД					
					mil_std_ch0.is_wait_cmd2 = 1;
					// Признаки ожидания ОС1, СД устанавливаются при приеме КС2
					break;
					
				case CMD_MSG_CNTRL_GROUP: // Формат 9
					msg_format_ch0 = 9;
					// Данные не ожидаются. Признаки не устанавливаются.
					break;
					
				case CMD_MSG_CNTRL_DATA_GRP: // Формат 10
					msg_format_ch0 = 10;
					// Ожидаем принять СД КУ
					mil_std_ch0.is_wait_cntrl_data = 1;
					break;
					
				default: msg_format_ch0 = 0xEF; // Error format
					break;
			} // switch(msg_reg_ch0)			
			cnt_of_words_ch0 = RING_GetCount(&udp_ring_tx);
			RING_Put(&udp_ring_tx, 0xFF); // Маркер
            RING_Put(&udp_ring_tx, 0x81);
			RING_Put(&udp_ring_tx, msg_format_ch0);
			RING_Put16(&udp_ring_tx, (uint16_t)cmd_word1_ch0);
		} // if(mil_std_ch0.is_wait_cmd1)
	}
	if(inten_reg & MIL_STD_1553_STATUS_VALMESS) // Успешно завершилась транзакция в канале (VALMESS)
	{
		errors_ch0 = 0;
		status_ch0 = status_reg; // Сохраняем статус для IDLE
		
		// Для сообщений формата 1, 2, 3, 7, 8 считываем СД
		if(mil_std_ch0.is_wait_data)
		{
			mil_std_ch0.is_wait_data = 0;
			mil_std_status.is_wait_data = 1;
			
			sub_addr = (cmd_word1_ch0 & 0x3E0) >> 5;
			sub_addr = sub_addr * 0x80;
			cnt_word = cmd_word1_ch0 & 0x1F;
			if(cnt_word == 0)
			{
				cnt_word = 32;
			}
			
			index = 0;
			do
			{
				cnt_word--;
				data_word = *((volatile unsigned int *)(MDR_MIL_STD_15532_BASE + sub_addr + index));
				index += 4;
				RING_Put16(&udp_ring_tx, (uint16_t)data_word);
			}while(cnt_word);
		}
		
		MDR_MIL_STD_15532->INTEN |= MIL_STD_1553_INTEN_IDLEIE; // Разрешаем прерывание по IDLE
	}
	if(inten_reg & MIL_STD_1553_STATUS_ERR) // Ошибка в сообщении (ERR)
	{
		if(!mil_std_ch0.is_correct_rx) // Не было получено достоверного слова
		{
			RING_Put(&udp_ring_tx, 0xFF); // Маркер
            RING_Put(&udp_ring_tx, 0x81);
			RING_Put(&udp_ring_tx, 0xEE); // Ошибка
		}
		status_ch0 = status_reg; // Сохраняем статус для IDLE
		errors_ch0 = MDR_MIL_STD_15532->ERROR;
		MDR_MIL_STD_15532->CONTROL &= ~MIL_STD_1553_CONTROL_RERR; // Сброс ошибок автоматически при установке бита IDLE
		MDR_MIL_STD_15532->INTEN |= MIL_STD_1553_INTEN_IDLEIE; // Разрешаем прерывание по IDLE
	}
	if(inten_reg & MIL_STD_1553_STATUS_IDLE) // IDLE
	{
		if(status_ch0 & MIL_STD_1553_STATUS_ERR) // Обмен завершен с ошибкой
		{
			MDR_MIL_STD_15532->CONTROL |= MIL_STD_1553_CONTROL_RERR; // Ошибки м.б сброшены только битом MR
		}
		MDR_MIL_STD_15532->INTEN &= ~MIL_STD_1553_INTEN_IDLEIE; // Запрещаем прерывания IDLE
		
		cnt_of_msg_ch0 = (cnt_of_msg_ch0 % 255) + 1; // cnt_of_msg_ch0 = [1:255]

		// Сохраняем флаги, статусы ошибок, счетчик сообщений и КС
        mil_std_flags = ((mil_std_status.is_correct_rx << 0)
                        |(mil_std_status.is_wait_cmd1 << 1)
                        |(mil_std_status.is_wait_cmd2 << 2)
                        |(mil_std_status.is_wait_reply1 << 3)
                        |(mil_std_status.is_wait_reply2 << 4)
                        |(mil_std_status.is_wait_cntrl_data << 5)
                        |(mil_std_status.is_wait_data << 6)
                        );
		RING_Put(&udp_ring_tx, mil_std_flags);
		RING_Put(&udp_ring_tx, (uint8_t)errors_ch0);
		RING_Put(&udp_ring_tx, cnt_of_msg_ch0);
        
		errors_ch0 = 0;
		
		// Определяем и сохраняем кол-во записанных слов
		cnt_of_words_ch0 = RING_GetCount(&udp_ring_tx) - cnt_of_words_ch0;
		RING_Put(&mil_std_cnt_word, cnt_of_words_ch0);
		cnt_of_words_ch0 = 0;
		
		// Сбрасываем флаги ожидания и статусы перед началом следующей транзакции
		MIL_STD_FlagsInit(&mil_std_ch0);
		MIL_STD_StatusInit(&mil_std_status);
	}
} // MIL_STD_1553B2_Handler()

///////////////////////////////////////////////////////////////////////////////
// Обработчик 1-ого контроллера резервного канала. Режим ОУ
void MIL_STD_1553B1_Handler(void)
{
	
}
