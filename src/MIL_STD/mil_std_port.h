/**
  ******************************************************************************
  * @file    ...\src\MIL_STD\mil_std_port.h
  * @date    03-04-2019
  * @brief   Содержит прототипы функций для настройки протокола MIL_STD
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MIL_STD_PORT_H
#define __MIL_STD_PORT_H

/* Includes ------------------------------------------------------------------*/
#include "MDR1986VE1T.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
// Определяем порядковые номера линий
#define PRDC_P      7
#define PRDC_N      8
#define PRMC_P      3
#define PRMC_N      4
#define PRD_PRMC	11

#define PRDB_P      3
#define PRDB_N      4
#define PRMB_P      15
#define PRMB_N      0
#define PRD_PRMB	6

// Битовые маски
#define PRDC_P_BIT      (1 << PRDC_P)
#define PRDC_N_BIT      (1 << PRDC_N)
#define PRMC_P_BIT      (1 << PRMC_P)
#define PRMC_N_BIT      (1 << PRMC_N)
#define PRD_PRMC_BIT	(1 << PRD_PRMC)

#define PRDB_P_BIT      (1 << PRDB_P)
#define PRDB_N_BIT      (1 << PRDB_N)
#define PRMB_P_BIT      (1 << PRMB_P)
#define PRMB_N_BIT      (1 << PRMB_N)
#define PRD_PRMB_BIT	(1 << PRD_PRMB)

// Задаем порт МК, на котором расположены линии MIL_STD
#define CLOCK_EN_MIL_STD_PORT_MAIN		RST_CLK_PER_CLOCK_PCLK_EN_PORTF
#define MIL_STD_PORT_MAIN				MDR_PORTF

#define CLOCK_EN_MIL_STD_PORT_RES		RST_CLK_PER_CLOCK_PCLK_EN_PORTD
#define MIL_STD_PORT_RES				MDR_PORTD

/* Exported functions -------------------------------------------------------*/
void MIL_STD_Cmd(MDR_MIL_STD_1553_TypeDef *MIL_STD_1553x, FunctionalState new_state);
void MIL_STD_PortConfig(void);
void MIL_STD_Config(void);

#endif // __MIL_STD_PORT_H

/************************ (C) COPYRIGHT *********************END OF FILE*******/
