/**
  ******************************************************************************
  * @file    ...\src\MIL_STD\mil_std_port.c
  * @date    03-04-2019
  * @brief   Содержит реализацию функций для настройки протокола MIL_STD
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "mil_std_port.h"

/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
static void inline MIL_STD_1553_DeInit(MDR_MIL_STD_1553_TypeDef * MIL_STD_1553x)
{
	MIL_STD_1553x->CONTROL = 1;
	MIL_STD_1553x->INTEN = 0;
	MIL_STD_1553x->CommandWord1 = 0;
	MIL_STD_1553x->CommandWord2 = 0;
	MIL_STD_1553x->ModeData = 0;
	MIL_STD_1553x->StatusWord1 = 0;
	MIL_STD_1553x->StatusWord2 = 0;
}

///////////////////////////////////////////////////////////////////////////////
void MIL_STD_Cmd(MDR_MIL_STD_1553_TypeDef *MIL_STD_1553x, FunctionalState new_state)
{
    if(new_state == ENABLE)
    {
        MIL_STD_1553x->CONTROL &= ~MIL_STD_1553_CONTROL_MR;
    }
    else
    {
        MIL_STD_1553x->CONTROL |= MIL_STD_1553_CONTROL_MR;
    }
}

///////////////////////////////////////////////////////////////////////////////
void MIL_STD_PortConfig(void)
{
    /*************************************************************************/
    // Настройка портов для 2-ого контроллера MIL_STD (основной канал)
    /*************************************************************************/
	// Вкл. тактирование порта
	MDR_RST_CLK->PER_CLOCK |= CLOCK_EN_MIL_STD_PORT_MAIN;
    
	// Режим работы - основная функция
	MIL_STD_PORT_MAIN->FUNC |=  (PORT_FUNC_MODE_MAIN << PORT_FUNC_MODE7_Pos)
                               |(PORT_FUNC_MODE_MAIN << PORT_FUNC_MODE8_Pos)
                               |(PORT_FUNC_MODE_MAIN << PORT_FUNC_MODE3_Pos)
                               |(PORT_FUNC_MODE_MAIN << PORT_FUNC_MODE4_Pos)
                               |(PORT_FUNC_MODE_MAIN << PORT_FUNC_MODE11_Pos);
    
	// Цифровой режим работы
    MIL_STD_PORT_MAIN->ANALOG |= (PRDC_P_BIT | PRDC_N_BIT | PRMC_P_BIT | PRMC_N_BIT | PRD_PRMC_BIT);
    
	// Направление пинов. PRD - на выход
	MIL_STD_PORT_MAIN->OE |= (PRDC_P_BIT | PRDC_N_BIT | PRD_PRMC_BIT);

	// Скорость изменения фронта - максимально быстрый
	MIL_STD_PORT_MAIN->PWR |=  (PORT_PWR_MAX_FAST << PORT_PWR7_Pos)
                              |(PORT_PWR_MAX_FAST << PORT_PWR8_Pos)
                              |(PORT_PWR_MAX_FAST << PORT_PWR3_Pos)
                              |(PORT_PWR_MAX_FAST << PORT_PWR4_Pos)
                              |(PORT_PWR_MAX_FAST << PORT_PWR11_Pos);

    /*************************************************************************/
    // Настройка портов для 1-ого контроллера MIL_STD (резервный канал)
    /*************************************************************************/
	// Вкл. тактирование порта
	MDR_RST_CLK->PER_CLOCK |= CLOCK_EN_MIL_STD_PORT_RES;
	MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_PORTC; // Для вывода PRMB_P
    
	// Режим работы - основная функция
	MIL_STD_PORT_RES->FUNC |=  (PORT_FUNC_MODE_MAIN << PORT_FUNC_MODE3_Pos)
                               |(PORT_FUNC_MODE_MAIN << PORT_FUNC_MODE4_Pos)
                               |(PORT_FUNC_MODE_MAIN << PORT_FUNC_MODE0_Pos)
                               |(PORT_FUNC_MODE_MAIN << PORT_FUNC_MODE6_Pos);
    MDR_PORTC->FUNC |= PORT_FUNC_MODE_MAIN << PORT_FUNC_MODE15_Pos; // Для вывода PRMB_P
    
	// Цифровой режим работы
    MIL_STD_PORT_RES->ANALOG |= (PRDB_P_BIT | PRDB_N_BIT | PRMB_N_BIT | PRD_PRMB_BIT);
    MDR_PORTC->ANALOG |= PRMB_P_BIT; // Для вывода PRMB_P
    
	// Направление пинов. PRD - на выход
	MIL_STD_PORT_RES->OE |= (PRDB_P_BIT | PRDB_N_BIT | PRD_PRMB_BIT);
    
	// Скорость изменения фронта - максимально быстрый
	MIL_STD_PORT_RES->PWR |=  (PORT_PWR_MAX_FAST << PORT_PWR3_Pos)
                              |(PORT_PWR_MAX_FAST << PORT_PWR4_Pos)
                              |(PORT_PWR_MAX_FAST << PORT_PWR0_Pos)
                              |(PORT_PWR_MAX_FAST << PORT_PWR6_Pos);
    MDR_PORTC->FUNC |= PORT_FUNC_MODE_MAIN << PORT_PWR15_Pos; // Для вывода PRMB_P
} // MIL_STD_PortConfig()

///////////////////////////////////////////////////////////////////////////////
void MIL_STD_Config(void)
{
    // Вкл. тактирование
    MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_MIL_STD_1553B2;    
    MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_MIL_STD_1553B1;
    
    // Настройка и разрешение тактирования. Частота MAN_CLK = 96/4 = 24 MHz
    // 0 - делитель 1; 1 - делитель 2; 2 - делитель 4
    MDR_RST_CLK->ETH_CLOCK |= (2 << RST_CLK_ETH_CLOCK_MAN_BRG_Pos) | RST_CLK_ETH_CLOCK_MAN_CLK_EN;
    
    // Сброс контроллеров
    MIL_STD_1553_DeInit(MDR_MIL_STD_15532);
    MIL_STD_1553_DeInit(MDR_MIL_STD_15531);    

    /************************* Настройка MIL_STD_MAIN. ***********************/
    // Сброс ошибок только битом MR. Разрешена фильтрация потока NRZ
    // Делитель частоты MAN_CLK = 24
    MDR_MIL_STD_15532->CONTROL |=    MIL_STD_1553_CONTROL_RERR
                                   | MIL_STD_1553_CONTROL_ENFILTER
                                   |(24 << MIL_STD_1553_CONTROL_DIV_Pos);
    // Установка в режим монитора 2-ого контроллера
    MDR_MIL_STD_15532->CONTROL |= (MIL_STD_1553_CONTROL_BCMODE | MIL_STD_1553_CONTROL_RTMODE);
        
    // Разрешение прерываний при приеме достоверного слова (RFLAGIE), 
    // при успешном завершении транзакции (VALMESSIE) и 
    // при возникновении ошибки в сообщении (ERRIE).
    // !!!Прерывание при неактивности в канале (IDlEIE) 
    // будем устанавливать по ходу обработки сообщений!!!
    MDR_MIL_STD_15532->INTEN |=   MIL_STD_1553_INTEN_RFLAGNIE 
                                | MIL_STD_1553_INTEN_VALMESSIE 
                                | MIL_STD_1553_INTEN_ERRIE;
    
    NVIC_EnableIRQ(MIL_STD_1553B2_IRQn); // Вкл. прерываний
    MIL_STD_Cmd(MDR_MIL_STD_15532, ENABLE); // Разрешение работы

    /************************** Настройка MIL_STD_RES. ***********************/
    // Сброс ошибок только битом MR. Разрешена фильтрация потока NRZ
    // Делитель частоты MAN_CLK = 24
    MDR_MIL_STD_15531->CONTROL |=    MIL_STD_1553_CONTROL_RERR
                                   | MIL_STD_1553_CONTROL_ENFILTER
                                   |(24 << MIL_STD_1553_CONTROL_DIV_Pos);
    // Установка в режим ОУ 1-ого контроллера
    MDR_MIL_STD_15531->CONTROL |= MIL_STD_1553_CONTROL_RTMODE;
    
    // Для ОУ обрабатываем прием достоверных слов, завершение транзакции
    // и ошибки в сообщении
    MDR_MIL_STD_15531->INTEN |=   MIL_STD_1553_INTEN_RFLAGNIE 
                                | MIL_STD_1553_INTEN_VALMESSIE 
                                | MIL_STD_1553_INTEN_ERRIE;
                                
    NVIC_EnableIRQ(MIL_STD_1553B1_IRQn); // Вкл. прерываний
    // Пока запрещаем работу. Включаем по команде управления!
    MIL_STD_Cmd(MDR_MIL_STD_15531, DISABLE);
}
/************************ (C) COPYRIGHT *********************END OF FILE*******/
