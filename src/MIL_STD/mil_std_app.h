/**
	*****************************************************************************
	* @file			.../src/MIL_STD/mil_std_app.h
	* @date			09-04-19
	* @brief		Содержит прототипы функций модуля mil_std_app
	*****************************************************************************
	*/

/* Define to prevent recursive inclusion ------------------------------------*/
#ifndef __MIL_STD_APP_H
#define __MIL_STD_APP_H

/* Includes -----------------------------------------------------------------*/
#include <stdint.h>

void MIL_STD_StructInit(void);
uint8_t MIL_STD_GetCountMSG(void);
void MIL_STD_DecrCountMSG(void);
void MIL_STD_SetCountMSG(uint8_t count);

#endif // __MIL_STD_APP_H
