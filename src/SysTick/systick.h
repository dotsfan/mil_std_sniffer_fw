/**
	*****************************************************************************
	* @file			...\src\SysTick\systick.h
	* @date			09-04-18
	* @brief		
	*****************************************************************************
	*/

/* Define to prevent recursive inclusion ------------------------------------*/
#ifndef __SYSTICK_H
#define __SYSTICK_H

/* Includes -----------------------------------------------------------------*/
#include "MDR1986VE1T.h"

/* Exported types -----------------------------------------------------------*/
/* Exported constants -------------------------------------------------------*/
/* Exported macro -----------------------------------------------------------*/
void SysTickInit(void);
void SysTickEnable(void);
void SysTickDisable(void);
uint32_t GetTick(void);

#endif // __SYSTICK_H
