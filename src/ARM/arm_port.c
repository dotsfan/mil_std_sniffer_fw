/**
	*****************************************************************************
	* @file			...\src\ARM\arm_port.c
	* @date			09-04-18
	* @brief		
	*****************************************************************************
	*/

/* Includes -----------------------------------------------------------------*/
#include "arm_port.h"
	
/* Private typedef ----------------------------------------------------------*/
/* Private define -----------------------------------------------------------*/
/* Private macro ------------------------------------------------------------*/
/* Private variables --------------------------------------------------------*/
/* Private function prototypes ----------------------------------------------*/
/* Private functions --------------------------------------------------------*/
void Eth_PortInit(void)
{
	MDR_RST_CLK->PER_CLOCK |= CLOCK_EN_ETH_LEDS_PORT;
	
	ETH_LEDS_PORT->ANALOG |= ETH_LEDS_PIN;
	
	ETH_LEDS_PORT->FUNC &= ~ETH_LEDS_FUNC;
	
	ETH_LEDS_PORT->OE |= ETH_LEDS_PIN;

	ETH_LEDS_PORT->PULL &= ~ETH_LEDS_PULL;
	
	ETH_LEDS_PORT->PD &= ~ETH_LEDS_PIN;
	
	ETH_LEDS_PORT->PWR |= ETH_LEDS_PWR;
	
	ETH_LEDS_PORT->RXTX &= ~ETH_LEDS_PIN;	
}

///////////////////////////////////////////////////////////////////////////////
void Eth_Link(void)
{
	// Индикация наличия link-сигнала (зеленый индикатор). 0 - вкл., 1 - выкл.
	if(MDR_ETHERNET1->PHY_Status & ETH_PHY_STATUS_LED1)
	{
		ETH_LEDS_PORT->CLRTX = ETH_LED_LINK_BIT;
        return;
	}    
    ETH_LEDS_PORT->SETTX = ETH_LED_LINK_BIT;
}



/************************ (C) COPYRIGHT *********************END OF FILE******/
