/**
	*****************************************************************************
	* @file			.../src/ARM/arm_port.h
	* @date			09-04-18
	* @brief		
	*****************************************************************************
	*/

/* Define to prevent recursive inclusion ------------------------------------*/
#ifndef __ARM_PORT_H
#define __ARM_PORT_H

/* Includes -----------------------------------------------------------------*/
#include "MDR1986VE1T.h"

/* Exported types -----------------------------------------------------------*/
/* Exported constants -------------------------------------------------------*/
/* Exported macro -----------------------------------------------------------*/
#define ETH_LED_LINK	(14)	// Желтый
#define ETH_LED_PHY		(15)	// Зеленый

#define ETH_LED_LINK_BIT	(1 << ETH_LED_LINK)
#define ETH_LED_PHY_BIT		(1 << ETH_LED_PHY)

#define CLOCK_EN_ETH_LEDS_PORT  RST_CLK_PER_CLOCK_PCLK_EN_PORTE
#define ETH_LEDS_PORT           MDR_PORTE

#define ETH_LEDS_PIN			(ETH_LED_LINK_BIT | ETH_LED_PHY_BIT)

#define ETH_LEDS_FUNC			((PORT_FUNC_MODE_Msk << ETH_LED_LINK*2) | (PORT_FUNC_MODE_Msk << ETH_LED_PHY*2))

#define ETH_LEDS_PULL			(ETH_LED_LINK_BIT | (1 << (ETH_LED_LINK + PORT_PULL_UP_Pos)) | \
													 ETH_LED_PHY_BIT  | (1 << (ETH_LED_PHY + PORT_PULL_UP_Pos)))

#define ETH_LEDS_PWR			((PORT_PWR_Msk << ETH_LED_LINK*2) | (PORT_PWR_Msk << ETH_LED_PHY*2))

/* Exported functions -------------------------------------------------------*/
void Eth_PortInit(void);
void Eth_Link(void);

#endif /* __ARM_PORT_H */

/************************ (C) COPYRIGHT *******END OF FILE********************/
