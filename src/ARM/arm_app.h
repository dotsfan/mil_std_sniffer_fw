/**
	*****************************************************************************
	* @file			.../src/ARM/arm_app.h
	*	@date			03-04-19
	* @brief		
	*****************************************************************************
	*/

/* Define to prevent recursive inclusion ------------------------------------*/
#ifndef __ARM_APP_H
#define __ARM_APP_H

/* Includes -----------------------------------------------------------------*/
#include <stdint.h>

void ARM_StructInit(void);
void ARM_DetectPacket(void);
void ARM_ParsePacketRx(void);
void ARM_ParsePacketTx(void);

#endif // __ARM_APP_H
