/**
	*****************************************************************************
	* @file			...\src\ARM\arm_app.c
	* @date			03-04-19
	* @brief		
	*****************************************************************************
	*/

/* Includes -----------------------------------------------------------------*/
#include "MDR1986VE1T.h"
#include "arm_app.h"
#include "global.h"
#include "ethfunc.h"
#include "ip.h"
#include "ICMP.h"
#include "arp.h"
#include "udp.h"
#include "ring_buffer.h"


/* Private variables --------------------------------------------------------*/
typedef struct
{
	__IO uint8_t is_udp_tx_err:1;
    __IO uint8_t is_run_enable:1;
	uint8_t RESERVED:6;
}arm_flags_t;

T_ARP_TABLE arp_table;
T_ETH_FRAME recv_data;
T_SOCK_ADDR client;
RING_buffer_t udp_ring_rx;
RING_buffer_t udp_ring_tx;
arm_flags_t arm_flags;

const uint16_t MCU_RUN_ENABLE = 0x0211;
const uint16_t MCU_RUN_DISABLE = 0x0210;

// Буферы приема/передачи по UDP (АРМ)
const uint16_t ARM_UDP_SIZE_TX = 128;
const uint16_t ARM_UDP_SIZE_RX = 32;
static uint8_t ARM_UdpTx[ARM_UDP_SIZE_TX] __attribute__ ((aligned (4))) = {0};
static uint8_t ARM_UdpRx[ARM_UDP_SIZE_RX] __attribute__ ((aligned (4))) = {0};

// Кольцевые буферы для хранения пакетов
const uint16_t ARM_RING_SIZE_TX = 256;
const uint16_t ARM_RING_SIZE_RX = 64;
static uint8_t ARM_RingTx[ARM_RING_SIZE_TX] __attribute__ ((aligned (4))) = {0};
static uint8_t ARM_RingRx[ARM_RING_SIZE_RX] __attribute__ ((aligned (4))) = {0};

extern uint32_t Board_IP;
extern uint16_t Board_Port;
extern RING_buffer_t mil_std_cnt_word;

extern uint8_t MIL_STD_GetCountMSG(void);
extern void MIL_STD_DecrCountMSG(void);
extern void MIL_STD_SetCountMSG(uint8_t count);

uint8_t arm_size_tx = 0;

///////////////////////////////////////////////////////////////////////////////
static void ARM_FlagsInit(void)
{
    arm_flags.is_udp_tx_err = 0;
    arm_flags.is_run_enable = 0;
    arm_flags.RESERVED = 0;
}

///////////////////////////////////////////////////////////////////////////////
void ARM_StructInit(void)
{
    ARM_FlagsInit();
    RING_Init(&udp_ring_rx, ARM_RingRx, ARM_RING_SIZE_RX);
    RING_Init(&udp_ring_tx, ARM_RingTx, ARM_RING_SIZE_TX);
}

///////////////////////////////////////////////////////////////////////////////
void ARM_DetectPacket(void)
{
	if(MDR_ETHERNET1->ETH_R_Head != MDR_ETHERNET1->ETH_R_Tail)
	{
		uint16_t size_eth = 0;
		uint16_t size_udp = 0;
		
		size_eth = ReadPacket(&recv_data);
		
		if(size_eth < ETHERNET_FRAME_LIMIT)
		{
			uint8_t ptr = 0;
			T_ARP_FRAME *parp;
			T_ICMP_FRAME *picmp;
			uint8_t *src_udp;
			
			switch(recv_data.eth_type) // Проверка протокола сети
			{
				case IP_PROTO:
					if((recv_data.ip_hdr.dst_ip == Board_IP) || ((recv_data.ip_hdr.dst_ip & 0xFF) == 0xFF)) // Our ip-address
					{
						if(CheckSum_IP((uint16_t*)&recv_data) == recv_data.ip_hdr.ip_chksum) // IP-checksum correct
						{
							if(recv_data.ip_hdr.protocol == ICMP_PROTO) // ICMP
							{
								picmp = (T_ICMP_FRAME*)&recv_data;
								picmp->size = recv_data.size;
								if(picmp->type == ICMP_REQ) // Echo request
								{
									ICMP_Reply(picmp);
								}
							}
							else if(recv_data.ip_hdr.protocol == UDP_PROTO) // UDP
							{
								if(recv_data.udp_hdr.dst_port == Board_Port) // Our port
								{
									src_udp = recv_data.udp_data;
                                    
                                    if((*src_udp == 0x9E) && (*(src_udp+1) == 0x3C) && (*(src_udp+2) == 0x79)) // Sync detect
                                    {
                                        // Запоминаем данные сокета
                                        client.ip = recv_data.ip_hdr.src_ip;
                                        client.port = recv_data.udp_hdr.src_port;
                                        
                                        size_udp = htons(recv_data.udp_hdr.length) - 8 /*udp hdr len*/;
                                        // Сохраняем принятые данные в буфере
                                        for(uint8_t i=0; i<size_udp; i++)
                                        {
                                            RING_Put(&udp_ring_rx, *src_udp++);
                                        }
                                    } // Synch detect
								} // Our port
							} // UDP
						} // // IP-checksum correct
					} // Our ip-address
					
					break; // IP_PROTO
						
				case ARP_PROTO:
					parp = (T_ARP_FRAME*)&recv_data;
					if(parp->req_rep == ARP_REQ) // ARP-request
					{
						if(parp->dst_ip == Board_IP) // Our ip
						{
							ARP_Reply(parp->src_eth, parp->src_ip);
						}
					}
					else // ARP-reply
					{
						// Look through ARP-table to find IP->MAC accordance
						ptr = 0;
						for(uint8_t i=0; i<arp_table.records; i++)
						{
							if(arp_table.table[i].ip != parp->src_ip)
							{
								ptr++;
								continue;
							}
							else // copy exists
							{
								break;
							}
						}
						if(ptr == arp_table.records) // no equal record exists (new reply came)
						{
							if(arp_table.records < ARP_TABLE_SIZE) // if there is space in ARP-table to add new ip->mac
								arp_table.records++;
							else
								ptr--;
							
							// Save new record
							arp_table.table[ptr].ip = parp->src_ip;
							arp_table.table[ptr].mac[0] = parp->src_eth[0];
							arp_table.table[ptr].mac[1] = parp->src_eth[1];
							arp_table.table[ptr].mac[2] = parp->src_eth[2];
						}
						
						if(arm_flags.is_udp_tx_err)
						{
							arm_flags.is_udp_tx_err = 0;
							if(UDP_Send(&client, ARM_UdpTx, arm_size_tx))
							{
								arm_flags.is_udp_tx_err = 1;
							}
						}
					}
					break; // ARP_PROTO
				
				default: break;
					
			} // switch(recv_data.eth_type)
			
		} // if(size_eth < ETHERNET_FRAME_LIMIT)
		else
		{
			/* Ignore too long frames */
			MDR_ETHERNET1->ETH_STAT = 0;  /* R_COUNT -= 1 */
		}
		
	} // if(MDR_ETHERNET1->ETH_R_Head != MDR_ETHERNET1->ETH_R_Tail)
	
} // ARM_DetectPacket()

///////////////////////////////////////////////////////////////////////////////
void ARM_ParsePacketRx(void)
{
    if(RING_GetCount(&udp_ring_rx))
    {
        if((RING_ShowSymbol(&udp_ring_rx, 0) == 0x9E) // Проверка синхрогруппы
         &&(RING_ShowSymbol(&udp_ring_rx, 1) == 0x3C)
         &&(RING_ShowSymbol(&udp_ring_rx, 2) == 0x79))
        {
            // Удаляем СГ
            RING_Pop(&udp_ring_rx);
            RING_Pop(&udp_ring_rx);
            RING_Pop(&udp_ring_rx);
            
            uint16_t arm_hdr_rx = RING_ShowSymbol(&udp_ring_rx, 0); // Читаем заголовок
            
            if(!((arm_hdr_rx == MCU_RUN_ENABLE) // Проверка заголовков
               ||(arm_hdr_rx == MCU_RUN_DISABLE)
                ))
            {
                RING_Pop16(&udp_ring_rx);
                return;                
            }
            
            uint8_t arm_crc_rx = 0;
            uint8_t arm_size_rx = (uint8_t)((arm_hdr_rx & 0xFF00) >> 8) + 1;
            for(uint8_t i=0; i<arm_size_rx; i++)
            {
                ARM_UdpRx[i] = RING_Pop(&udp_ring_rx);
                arm_crc_rx ^=  ARM_UdpRx[i];
            }
            
            if(arm_crc_rx)
                return;
            
            // Обработка команд от АРМ
            switch(arm_hdr_rx)
            {
                case MCU_RUN_ENABLE:
                    arm_flags.is_run_enable = 1;
                    // Очищаем буфер
                    RING_Clear(&udp_ring_tx);
                    // Формируем пакет с подтверждением запуска монитора
                    RING_Put(&udp_ring_tx, 0xFF); // СИ
                    RING_Put(&udp_ring_tx, 0x81);
                    RING_Put(&udp_ring_tx, 0xF1); // Код команды
					RING_Put(&udp_ring_tx, 0x00); // Дополняем сообщение до минимального размера
					RING_Put(&udp_ring_tx, 0x00);
					RING_Put(&udp_ring_tx, 0x00);
                    RING_Put(&udp_ring_tx, 0x8F); // КС
                    // Установим счетчик в 1, чтобы передать 1 пакет
                    MIL_STD_SetCountMSG(1);
                    break;
                
                case MCU_RUN_DISABLE:
                    arm_flags.is_run_enable = 0;
                
                default: break;
            }
        }
        else
        {
            RING_Pop(&udp_ring_rx);
        }
    }
    
} // ARM_ParsePacketRx()

///////////////////////////////////////////////////////////////////////////////
void ARM_ParsePacketTx(void)
{
    if(RING_GetCount(&udp_ring_tx) && MIL_STD_GetCountMSG())
    {
        if(!arm_flags.is_run_enable) // Монитор не активен
        {
            // Очищаем буфер и счетчики
            RING_Clear(&udp_ring_tx);
            MIL_STD_SetCountMSG(0);
            return;
        }
        
        // Считываем кол-во записанных байт за транзакцию MIL_STD
        arm_size_tx = RING_Pop(&mil_std_cnt_word);
        // Формируем буфер и считаем КС
        uint8_t arm_crc_tx = 0;
        for(uint8_t i=0; i<arm_size_tx; i++)
        {
            ARM_UdpTx[i] = RING_Pop(&udp_ring_tx);
            arm_crc_tx ^= ARM_UdpTx[i];
        }
        ARM_UdpTx[arm_size_tx++] = arm_crc_tx;
                
        if(UDP_Send(&client, ARM_UdpTx, arm_size_tx))
        {
            arm_flags.is_udp_tx_err = 1;
        }
        
        MIL_STD_DecrCountMSG();
    }
} // ARM_ParsePacketTx()
