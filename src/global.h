/**
  *****************************************************************************
  * @file    ...\src\global.h
  * @date    09-04-2018
  * @brief   
  *****************************************************************************
  */

/* Define to prevent recursive inclusion ------------------------------------*/
#ifndef __GLOBAL_H
#define __GLOBAL_H

/* Includes -----------------------------------------------------------------*/

#define IP_PROTO							0x0008
#define UDP_PROTO							0x11
#define ICMP_PROTO						0x01
#define ARP_PROTO							0x0608
#define ARP_REP								0x0200
#define ARP_REQ								0x0100
#define ETH_PROTO							0x0100
#define ICMP_REQ							0x08
#define ICMP_REP							0x00

#define htons(wrd) 						__REV16(wrd)//(wrd << 8 | wrd >> 8)

#endif // __GLOBAL_H
