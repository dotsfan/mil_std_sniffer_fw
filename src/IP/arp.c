/**
	*****************************************************************************
	* @file			...\src\IP\Arp.c
	* @date			09-04-18
	* @brief		
	*****************************************************************************
	*/

/* Includes -----------------------------------------------------------------*/
#include "arp.h"
#include "global.h"
#include "systick.h"

extern uint32_t Board_IP;

///////////////////////////////////////////////////////////////////////////////
void ARP_Request(uint32_t ip)
{
	T_ARP_FRAME req;
	req.dst_mac[0] = 0xFFFF;
	req.dst_mac[1] = 0xFFFF;
	req.dst_mac[2] = 0xFFFF;
	req.src_mac[0] = MDR_ETHERNET1->ETH_MAC_T;
	req.src_mac[1] = MDR_ETHERNET1->ETH_MAC_M;
	req.src_mac[2] = MDR_ETHERNET1->ETH_MAC_H;
	req.eth_type = ARP_PROTO;
	req.hard_type = ETH_PROTO;
	req.prot_type = IP_PROTO;
	req.hard_size = 0x06;
	req.prot_size = 0x04;
	req.req_rep = ARP_REQ;
	req.src_eth[0] = MDR_ETHERNET1->ETH_MAC_T;
	req.src_eth[1] = MDR_ETHERNET1->ETH_MAC_M;
	req.src_eth[2] = MDR_ETHERNET1->ETH_MAC_H;
	req.src_ip = Board_IP;
	req.dst_eth[0] = 0x0000;
	req.dst_eth[1] = 0x0000;
	req.dst_eth[2] = 0x0000;
	req.dst_ip = ip;
	
	SendPacket(&req, sizeof(req));
}

///////////////////////////////////////////////////////////////////////////////
void ARP_Reply(uint16_t* mac, uint32_t ip)
{
	T_ARP_FRAME rep;
	rep.dst_mac[0] = mac[0];
	rep.dst_mac[1] = mac[1];
	rep.dst_mac[2] = mac[2];
	rep.src_mac[0] = MDR_ETHERNET1->ETH_MAC_T;
	rep.src_mac[1] = MDR_ETHERNET1->ETH_MAC_M;
	rep.src_mac[2] = MDR_ETHERNET1->ETH_MAC_H;
	rep.eth_type = ARP_PROTO;
	rep.hard_type = ETH_PROTO;
	rep.prot_type = IP_PROTO;
	rep.hard_size = 0x06;
	rep.prot_size = 0x04;
	rep.req_rep = ARP_REP;
	rep.src_eth[0] = MDR_ETHERNET1->ETH_MAC_T;
	rep.src_eth[1] = MDR_ETHERNET1->ETH_MAC_M;
	rep.src_eth[2] = MDR_ETHERNET1->ETH_MAC_H;
	rep.src_ip = Board_IP;
	rep.dst_eth[0] = mac[0];
	rep.dst_eth[1] = mac[1];
	rep.dst_eth[2] = mac[2];
	rep.dst_ip = ip;
	
	SendPacket(&rep, sizeof(rep));
}
