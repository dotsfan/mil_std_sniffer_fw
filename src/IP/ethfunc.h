/**
	*****************************************************************************
	* @file			...\src\IP\ethfunc.h
	* @date			09-04-18
	* @brief		
	*****************************************************************************
	*/

/* Define to prevent recursive inclusion ------------------------------------*/
#ifndef __ETHFUNC_H
#define __ETHFUNC_H

/* Includes -----------------------------------------------------------------*/
#include "tcp_ip_structs.h"

#define ETHERNET_FRAME_LIMIT     1518
#define ETHERNET_PAYLOAD_LIMIT   1500

void ETHERNET_Config(void);
uint32_t ReadPacket(T_ETH_FRAME *Frame);
int	SendPacket(void*, int);

#endif	//__ETHFUNC_H
