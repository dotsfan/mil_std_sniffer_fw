/**
	*****************************************************************************
	* @file			...\src\IP\udp.h
	* @date			09-04-18
	* @brief		
	*****************************************************************************
	*/

/* Define to prevent recursive inclusion ------------------------------------*/
#ifndef __UDP_H
#define __UDP_H

/* Includes -----------------------------------------------------------------*/
#include "tcp_ip_structs.h"

int8_t UDP_Send(T_SOCK_ADDR* to, uint8_t *data, uint16_t length);
void UDP_Init(void);

#endif // __UDP_H
