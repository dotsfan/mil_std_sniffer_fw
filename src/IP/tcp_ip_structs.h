/**
	*****************************************************************************
	* @file			.../src/IP/tcp_ip_structs.h
	*	@date			09-04-18
	* @brief		
	*****************************************************************************
	*/

/* Define to prevent recursive inclusion ------------------------------------*/
#ifndef __STRUCTS_H
#define __STRUCTS_H

/* Includes -----------------------------------------------------------------*/
#include <stdint.h>

#pragma pack(1)

#define ARP_TABLE_SIZE			32

///////////////////////////////////////////////////////////////////////////////
typedef struct
{
	uint8_t version_len;			//0x45 Always
	uint8_t tos;							//type of service
	uint16_t length;					//datagramm length
	uint16_t id;
	uint16_t offset;					//always will be 0
	uint8_t ttl;							//time to live
	uint8_t protocol;					//ip, arp or others
	uint16_t ip_chksum;
	uint32_t src_ip;
	uint32_t dst_ip;
	
} T_IP_HDR;

///////////////////////////////////////////////////////////////////////////////
typedef struct
{
	uint16_t	src_port;
	uint16_t	dst_port;
	uint16_t	length;
	uint16_t	chk_sum;
} T_UDP_HDR;

///////////////////////////////////////////////////////////////////////////////
typedef struct
{
	uint16_t 	dst_mac[3];
	uint16_t 	src_mac[3];
	uint16_t	eth_type;
	T_IP_HDR	ip_hdr;
	T_UDP_HDR	udp_hdr;
	uint8_t		udp_data[1024];
	uint16_t	size;
} T_ETH_FRAME;

///////////////////////////////////////////////////////////////////////////////
typedef struct
{
	uint16_t 	dst_mac[3];
	uint16_t 	src_mac[3];
	uint16_t	eth_type;
	uint16_t	hard_type;
	uint16_t	prot_type;
	uint8_t		hard_size;
	uint8_t		prot_size;
	uint16_t	req_rep;
	uint16_t	src_eth[3];
	uint32_t	src_ip;
	uint16_t	dst_eth[3];
	uint32_t	dst_ip;
	
} T_ARP_FRAME;

///////////////////////////////////////////////////////////////////////////////
typedef struct
{
	uint16_t 	dst_mac[3];
	uint16_t 	src_mac[3];
	uint16_t	eth_type;
	T_IP_HDR	ip_hdr;
	uint8_t		type;
	uint8_t		code;
	uint16_t	icmp_chksum;
	uint16_t	id;
	uint16_t	number;
	uint8_t		data[128];
	uint16_t	size;
	
} T_ICMP_FRAME;

#pragma pack(8)
///////////////////////////////////////////////////////////////////////////////
typedef struct 
{
	uint16_t mac[3];
	uint32_t ip;
} T_ARP_REC;

///////////////////////////////////////////////////////////////////////////////
typedef struct
{
	T_ARP_REC table[ARP_TABLE_SIZE];
	uint8_t		records;
} T_ARP_TABLE;

///////////////////////////////////////////////////////////////////////////////
typedef struct
{
	uint32_t		ip;
	uint16_t		port;
} T_SOCK_ADDR;
//#pragma pack(pop)

#endif // __STRUCTS_H
