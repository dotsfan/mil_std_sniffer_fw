/**
	*****************************************************************************
	* @file			.../src/IP/ICMP.c
	*	@date			09-04-18
	* @brief		
	*****************************************************************************
	*/

/* Includes -------------------------------------------------------------------*/
#include "ICMP.h"
#include "global.h"
#include "ip.h"
#include "MDR1986VE1T.h"

extern uint32_t Board_IP;

static uint16_t CheckSum_ICMP(uint16_t size, T_ICMP_FRAME* data);

///////////////////////////////////////////////////////////////////////////////
static uint16_t CheckSum_ICMP(uint16_t size, T_ICMP_FRAME* data)
{
	uint32_t i, cs = 0;
	uint16_t *ptr = (((uint16_t*)data) + 17);
	size = (size + 1)>>1;
  for(i = 0;i < size; i++)
  {
		if(i == 1)
			continue;
		else
			cs += *(ptr + i);
   }
    cs = (cs >> 16) + (cs & 0xFFFF);
        return (uint16_t)(~cs);
}

///////////////////////////////////////////////////////////////////////////////
void ICMP_Reply(T_ICMP_FRAME *req)
{
	uint32_t i;
	uint16_t size;
	uint16_t *src, *dst;
	T_ICMP_FRAME icmp_rep;
	
	icmp_rep.dst_mac[0] = req->src_mac[0];
	icmp_rep.dst_mac[1] = req->src_mac[1];
	icmp_rep.dst_mac[2] = req->src_mac[2];
	icmp_rep.src_mac[0] = MDR_ETHERNET1->ETH_MAC_T;
	icmp_rep.src_mac[1] = MDR_ETHERNET1->ETH_MAC_M;
	icmp_rep.src_mac[2] = MDR_ETHERNET1->ETH_MAC_H;
	icmp_rep.eth_type = IP_PROTO;
	icmp_rep.ip_hdr.version_len = 0x45;
	icmp_rep.ip_hdr.tos = 0x00;
	
	icmp_rep.ip_hdr.length = req->ip_hdr.length;
	icmp_rep.ip_hdr.id = req->ip_hdr.id;
	icmp_rep.ip_hdr.offset = 0x0000;
	icmp_rep.ip_hdr.ttl = 0x80;
	icmp_rep.ip_hdr.protocol = ICMP_PROTO;
	icmp_rep.ip_hdr.src_ip = Board_IP;
	icmp_rep.ip_hdr.dst_ip = req->ip_hdr.src_ip;
	icmp_rep.ip_hdr.ip_chksum = CheckSum_IP(((uint16_t*)&icmp_rep));
	
	icmp_rep.type = ICMP_REP;
	icmp_rep.code = req->code;
	icmp_rep.id = req->id;
	icmp_rep.number = req->number;
	src = (uint16_t*)req->data;
	dst = (uint16_t*)icmp_rep.data;
	size = (htons(req->ip_hdr.length) - sizeof(T_IP_HDR) + 1) >> 1;
	for(i = 0;i < size;i++)
	{
		*dst++ = *src++;
	}
	icmp_rep.icmp_chksum = CheckSum_ICMP(htons(req->ip_hdr.length) - sizeof(T_IP_HDR), &icmp_rep);
	SendPacket(&icmp_rep, req->size - 4);
}
