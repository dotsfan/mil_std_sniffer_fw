/**
	*****************************************************************************
	* @file			...\src\IP\arp.h
	* @date			09-04-18
	* @brief		
	*****************************************************************************
	*/

/* Define to prevent recursive inclusion ------------------------------------*/
#ifndef __ARP_H
#define __ARP_H

/* Includes -----------------------------------------------------------------*/
#include "ethfunc.h"
#include "tcp_ip_structs.h"

void ARP_Request(uint32_t ip);
void ARP_Reply(uint16_t*, uint32_t);

#endif // __ARP_H
