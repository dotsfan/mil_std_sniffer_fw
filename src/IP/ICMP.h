/**
	*****************************************************************************
	* @file			...\src\IP\ICMP.h
	* @date			09-04-18
	* @brief		
	*****************************************************************************
	*/

/* Define to prevent recursive inclusion ------------------------------------*/
#ifndef __ICMP_H
#define __ICMP_H

/* Includes -----------------------------------------------------------------*/
#include "ethfunc.h"
#include "tcp_ip_structs.h"

void ICMP_Reply(T_ICMP_FRAME* );

#endif // __ICMP_H
