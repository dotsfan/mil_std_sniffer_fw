/**
	*****************************************************************************
	* @file			.../src/IP/ethfunc.c
	*	@date			09-04-18
	* @brief		
	*****************************************************************************
	*/

/* Includes -----------------------------------------------------------------*/
#include "ethfunc.h"
#include "MDR1986VE1T.h"

#define PHY_A                     0x1CU

/* Private functions prototypes ---------------------------------------------*/
static void FlushFIFO(void);
static void PHY_Init(void);
static void MAC_Reset(void);

uint16_t Board_MAC[3] = {0x3402, 0x7856, 0xBC9A};	// Board MAC address

/* Private functions declarations -------------------------------------------*/
static void FlushFIFO(void)
{
	uint32_t *ptr;
	ptr = (uint32_t*)MDR_ETHERNET1_BUF_BASE;
	
	for(uint32_t i=0; i < (MDR_ETHERNET1_BUF_SIZE); i++)
		*ptr++ = 0;
	__DMB();
}

///////////////////////////////////////////////////////////////////////////////
static void PHY_Init(void)
{
	// PHY_CLK_SEL = HSE2, ETH_CLK_EN=1, PHY_CLK_EN=1, ETH_CLK = 25MHz
	MDR_RST_CLK->ETH_CLOCK &= ~(RST_CLK_ETH_CLOCK_PHY_BRG_Msk |
															RST_CLK_ETH_CLOCK_PHY_CLK_SEL_Msk);
		
	// Выбираем источник HSE2 для Ethernet PHY	
	MDR_RST_CLK->ETH_CLOCK |=
		(RST_CLK_ETH_CLOCK_PHY_BRG_PHY1_CLK << RST_CLK_ETH_CLOCK_PHY_BRG_Pos) |
		(1 << RST_CLK_ETH_CLOCK_ETH_CLK_EN_Pos) |
		(1 << RST_CLK_ETH_CLOCK_PHY_CLK_EN_Pos) |
		(RST_CLK_ETH_CLOCK_PHY_CLK_SEL_HSE2 << RST_CLK_ETH_CLOCK_PHY_CLK_SEL_Pos);
	
	// Устанавливаем режим работы 100Мбит/FD
	MDR_ETHERNET1->PHY_Control =
		(1 << ETH_PHY_CONTROL_nRST_Pos) |
		(ETH_PHY_CONTROL_MODE_100M_FD << ETH_PHY_CONTROL_MODE_Pos) |
		(0 << ETH_PHY_CONTROL_FX_EN_Pos) |
		(0 << ETH_PHY_CONTROL_MDI_Pos) |
		(0 << ETH_PHY_CONTROL_MDIO_SEL_Pos) |
		(0 << ETH_PHY_CONTROL_MDC_Pos) |
		(PHY_A << ETH_PHY_CONTROL_PHYADD_Pos);	

	__DMB();
	
	// Ждем пока модуль в состоянии сброса
	while((MDR_ETHERNET1->PHY_Status & ETH_PHY_STATUS_READY) == 0){}

}

///////////////////////////////////////////////////////////////////////////////
static void MAC_Reset(void)
{
	MDR_ETHERNET1->ETH_G_CFGh |= (ETH_G_CFGh_XRST | ETH_G_CFGh_RRST);

	FlushFIFO();

	MDR_ETHERNET1->ETH_Dilimiter = 0x1000;	//4096 receive and transmit buffers

	MDR_ETHERNET1->ETH_HASH0 = 0x0000;
	MDR_ETHERNET1->ETH_HASH1 = 0x0000;
	MDR_ETHERNET1->ETH_HASH2 = 0x0000;
	MDR_ETHERNET1->ETH_HASH3 = 0x8000;

	MDR_ETHERNET1->ETH_IPG = 0x0060;
	MDR_ETHERNET1->ETH_PSC = 0x0050;
	MDR_ETHERNET1->ETH_BAG = 0x0200;
	MDR_ETHERNET1->ETH_JitterWnd = 0x0005;
	MDR_ETHERNET1->ETH_R_CFG = 0x8406;		//
	MDR_ETHERNET1->ETH_X_CFG = 0x81FF;

	/* Set FIFO mode, collision window */
	MDR_ETHERNET1->ETH_G_CFGl = (
	  ( ETH_G_CFGl_BUFF_MODE_LIN << ETH_G_CFGl_BUFF_MODE_Pos )
	| ( 128   << ETH_G_CFGl_ColWnd_Pos )
	);
	

	/* Support debug in FIFO mode */
	MDR_ETHERNET1->ETH_G_CFGh = ETH_G_CFGh_DBG_XF_EN | ETH_G_CFGh_DBG_RF_EN;

	MDR_ETHERNET1->ETH_IMR = 0;
	MDR_ETHERNET1->ETH_IFR = 0xFFFF;

	MDR_ETHERNET1->ETH_R_Head = 0x0000;
	MDR_ETHERNET1->ETH_X_Tail = 0x1000;

	MDR_ETHERNET1->ETH_G_CFGh &= ~(ETH_G_CFGh_XRST | ETH_G_CFGh_RRST);
	
	__DMB();
}

/* Private functions declarations -------------------------------------------*/
void ETHERNET_Config(void)
{
	PHY_Init();

	MDR_ETHERNET1->ETH_MAC_T = Board_MAC[0];
	MDR_ETHERNET1->ETH_MAC_M = Board_MAC[1];
	MDR_ETHERNET1->ETH_MAC_H = Board_MAC[2];
	
	__DMB();
	
	MAC_Reset();
	
	MDR_ETHERNET1->ETH_IMR = 0x0000;	//No interrupts allow
	
	__DMB();
}

///////////////////////////////////////////////////////////////////////////////
uint32_t ReadPacket(T_ETH_FRAME* Frame)
{
  uint16_t space_start = 0,space_end = 0,tail,head;
	uint32_t *src, *dst;
	uint32_t size, i;
	uint16_t tmp[2];

	tail=MDR_ETHERNET1->ETH_R_Tail;
	head=MDR_ETHERNET1->ETH_R_Head;
	

	if(tail > head)
	{
		space_end = tail - head;
		space_start = 0;
	}
	else
	{
		space_end = 0x1000 - head;
		space_start = tail;
	}

	src = (uint32_t*)(MDR_ETHERNET1_BUF_BASE + head);
	dst = (uint32_t*)(Frame);

	*((uint32_t*)tmp) = *src++;	//number of bytes in received packet
	space_end -= 4;
	if((uint16_t)src > 0xFFF)
		src = (uint32_t*)MDR_ETHERNET1_BUF_BASE;

	size = (tmp[0] + 3) / 4;
	if(tmp[0] <= space_end)
	{
		for(i = 0;i < size;i++)
			*dst++ = *src++;
	}
	else
	{
		size = size - space_end / 4;
		for(i = 0; i < (space_end / 4); i++)
			*dst++ = *src++;
		src = (uint32_t*)MDR_ETHERNET1_BUF_BASE;
		for(i = 0;i < size;i++)
			*dst++ = *src++;
	}
	if((uint16_t)src > 0xFFF)	
		src = (uint32_t*)MDR_ETHERNET1_BUF_BASE;

	MDR_ETHERNET1->ETH_R_Head = (uint16_t)src;
	MDR_ETHERNET1->ETH_STAT -= 0x20;
	__DMB();
	Frame->size = tmp[0];
	
	return tmp[0];
} // ReadPacket()

///////////////////////////////////////////////////////////////////////////////
int	SendPacket(void* buffer, int size)
{
	uint16_t i;
	uint32_t tmp, head, tail;
	uint32_t *src, *dst;
	uint16_t space[2];

	head = MDR_ETHERNET1->ETH_X_Head;
	tail = MDR_ETHERNET1->ETH_X_Tail;

	//amount of free space in buffer
	if(head > tail)
	{
		space[0] = head - tail;
		space[1] = 0;
	} 
	else
	{
		space[0] = MDR_ETHERNET1_BUF_SIZE - tail;
		space[1] = head - 0x1000;
	}

	if(size > (space[0] + space[1] - 8))
		return 0;	//if there is not enough space in buffer

	tmp = size;
	src = buffer;
	dst = (uint32_t*)(MDR_ETHERNET1_BUF_BASE + tail);
	
	*dst++ = tmp;
	__DMB();
	space[0] -= 4;
	if((uint16_t)dst > (MDR_ETHERNET1_BUF_SIZE-4))
		dst = (uint32_t*)0x38001000;

	tmp = (size + 3) / 4;

	if(size <= space[0])
	{
		for(i = 0;i < tmp;i++)
		{
			*dst = *src;
			__DMB();
			dst++;
			src++;
			
		}
	}
	else
	{
		tmp -= space[0] / 4;
		for(i = 0;i < (space[0] / 4);i++)
		{
			*dst = *src;
			__DMB();
			dst++;
			src++;
			
		}
		dst = (uint32_t*)0x38001000;
		for(i = 0;i < tmp;i++)
		{
			*dst = *src;
			__DMB();
			dst++;
			src++;
			
		}
	}
	__DMB();
	if((uint16_t)dst > (MDR_ETHERNET1_BUF_SIZE-4))	
		dst = (uint32_t*)0x38001000;
	tmp = 0;
	*dst++ = tmp;
	__DMB();
	if((uint16_t)dst > (MDR_ETHERNET1_BUF_SIZE-4))
		dst = (uint32_t*)0x38001000;
	__DMB();
	MDR_ETHERNET1->ETH_X_Tail = (uint16_t)dst;
	__DMB();
	return	size;
} // SendPacket()
