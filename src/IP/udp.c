/**
	*****************************************************************************
	* @file			.../src/IP/udp.c
	*	@date			09-04-18
	* @brief		
	*****************************************************************************
	*/

/* Includes -----------------------------------------------------------------*/
#include "udp.h"
#include "global.h"
#include "ethfunc.h"
#include "arp.h"
#include "ip.h"
#include "systick.h"

#define ARP_WAIT							100 // wait 1 second for arp reply

static __IO uint32_t arp_tick = 0;
static __IO uint8_t arp_attempts = 0;

extern T_ARP_TABLE arp_table;

uint32_t Board_IP = 0x0F01A8C0; // 192.168.1.15
uint16_t Board_Port = 0xA00F; // 4000

///////////////////////////////////////////////////////////////////////////////
void UDP_Init(void)
{
	arp_attempts = 0;
	arp_tick = 0;
}

///////////////////////////////////////////////////////////////////////////////
int8_t UDP_Send(T_SOCK_ADDR* to, uint8_t *data, uint16_t length)
{
	T_ETH_FRAME send_data;
	
	int16_t i, rec_num;
	uint32_t size;
	uint16_t *src, *dst;
	
	rec_num = -1;
	//Look ARP table through
	for(i = 0;i < arp_table.records;i++)
	{
		if(to->ip == arp_table.table[i].ip)
		{
			rec_num = i;
			break;
		}
	}

	//if there is no appropriate record in ARP-table
	if(rec_num < 0)
	{
		SysTickEnable();											//turn system timer on
		if(GetTick() - arp_tick > ARP_WAIT)
		{
			if(arp_attempts < 3)
			{
				ARP_Request(to->ip);
				arp_attempts++;
				arp_tick = GetTick();
				return -2; // 0
			}
			else
			{
				arp_attempts = 0;
				arp_tick = 0;
				return -1;
			}
				
		}
		else
		{
			return 0;
		}
	}
	
	SysTickDisable();
	arp_attempts = 0;
	
	send_data.src_mac[0] = MDR_ETHERNET1->ETH_MAC_T;
	send_data.src_mac[1] = MDR_ETHERNET1->ETH_MAC_M;
	send_data.src_mac[2] = MDR_ETHERNET1->ETH_MAC_H;
	send_data.dst_mac[0] = arp_table.table[rec_num].mac[0];
	send_data.dst_mac[1] = arp_table.table[rec_num].mac[1];
	send_data.dst_mac[2] = arp_table.table[rec_num].mac[2];
	send_data.eth_type = IP_PROTO;
	send_data.ip_hdr.version_len = 0x45;
	send_data.ip_hdr.tos = 0x00;
	send_data.ip_hdr.length = htons((20 + 8 + length));
	send_data.ip_hdr.id = 0x6F08;
	send_data.ip_hdr.offset = 0x0000;
	send_data.ip_hdr.ttl = 0x80;
	send_data.ip_hdr.protocol = UDP_PROTO;
	send_data.ip_hdr.src_ip = Board_IP;
	send_data.ip_hdr.dst_ip = to->ip;
	send_data.ip_hdr.ip_chksum = CheckSum_IP(((uint16_t*)&send_data));
	send_data.udp_hdr.dst_port = to->port;
	send_data.udp_hdr.src_port = Board_Port;
	send_data.udp_hdr.length = htons((8 + length));
	send_data.udp_hdr.chk_sum = 0x0000;
	
	
	size = (length + 1) >> 1;
	
	dst = (uint16_t*)send_data.udp_data;
	src = (uint16_t*)data;
	for(i = 0;i < size;i++)
	{
		*dst++ = *src++;
//		*((uint8_t*)send_data.udp_data + i) = *((uint8_t*)data + i);
	}
	send_data.size = 6 + 6 + 2 + 20 + 8 + length;
	SendPacket(&send_data, send_data.size);
	return length;
}
