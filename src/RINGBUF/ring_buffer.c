/**
  ******************************************************************************
  * @file    ...\src\RINGBUF\ring_buffer.c 
  * @date    28-02-2019
  * @brief   Содержит реализацию модуля кольцевого буфера
  ******************************************************************************
  */

#include "ring_buffer.h"

///////////////////////////////////////////////////////////////////////////////
RING_ErrorStatus_t RING_Init(RING_buffer_t *ring, uint8_t *buf, uint16_t size)
{
    ring->size = size;
    ring->buffer = buf;
    RING_Clear(ring);

    return (ring->buffer ? RING_SUCCESS : RING_ERROR);
}

///////////////////////////////////////////////////////////////////////////////
uint16_t RING_GetCount(const RING_buffer_t *buf)
{
    uint16_t retval = 0;
    if (buf->idx_wr < buf->idx_rd) retval = buf->size + buf->idx_wr - buf->idx_rd;
    else retval = buf->idx_wr - buf->idx_rd;
    return retval;
}

///////////////////////////////////////////////////////////////////////////////
void RING_Clear(RING_buffer_t* buf)
{
    buf->idx_wr = 0;
    buf->idx_rd = 0;
}

///////////////////////////////////////////////////////////////////////////////
void RING_Put( RING_buffer_t* buf, uint8_t symbol)
{
    buf->buffer[buf->idx_wr++] = symbol;
    if (buf->idx_wr >= buf->size) buf->idx_wr = 0;
}

///////////////////////////////////////////////////////////////////////////////
void RING_Put16( RING_buffer_t* buf, uint16_t symbol)
{
    buf->buffer[buf->idx_wr++] = symbol >> 8;
    if (buf->idx_wr >= buf->size) buf->idx_wr = 0;
    buf->buffer[buf->idx_wr++] = symbol & 0xFF;
    if (buf->idx_wr >= buf->size) buf->idx_wr = 0;
}

///////////////////////////////////////////////////////////////////////////////
uint8_t RING_Pop(RING_buffer_t *buf)
{
    uint8_t retval = buf->buffer[buf->idx_rd++];
    if (buf->idx_rd >= buf->size) buf->idx_rd = 0;
    return retval;
}

///////////////////////////////////////////////////////////////////////////////
uint16_t RING_Pop16(RING_buffer_t *buf)
{
    uint16_t retval = RING_Pop(buf) << 8;
    retval += RING_Pop(buf);
    return retval;
}

///////////////////////////////////////////////////////////////////////////////
int32_t RING_ShowSymbol(const RING_buffer_t *buf, uint16_t symbolNumber)
{
    uint32_t pointer = buf->idx_rd + symbolNumber;
    int32_t  retval = -1;
    if (symbolNumber < RING_GetCount(buf))
    {
        if (pointer > buf->size) pointer -= buf->size;
        retval = buf->buffer[ pointer ] ;
    }
    return retval;
}
