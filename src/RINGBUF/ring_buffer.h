/**
  ******************************************************************************
  * @file    ...\src\RINGBUF\ring_buffer.h
  * @date    28-02-2019
  * @brief   Содержит прототипы функций кольцевого буфера
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef RING_BUFFER_H
#define RING_BUFFER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stdint.h"

// Тип, описывающий кольцевой буфер
typedef struct
{
    uint8_t *buffer;    // Указатель на буфер-хранилище
    uint16_t idx_wr;    // Номер ячейки для записи
    uint16_t idx_rd;    // Номер ячейки для чтения
    uint16_t size;      // Размер буфера-хранилища
}RING_buffer_t;

// Состояния выполнения функции инициализации
typedef enum
{
    RING_ERROR = 0,
    RING_SUCCESS = !RING_ERROR
}RING_ErrorStatus_t;

/* Exported functions -------------------------------------------------------*/
RING_ErrorStatus_t RING_Init(RING_buffer_t *ring, uint8_t *buf, uint16_t size);
uint16_t RING_GetCount(const RING_buffer_t *buf);
void RING_Clear(RING_buffer_t* buf);
void RING_Put( RING_buffer_t* buf, uint8_t symbol);
void RING_Put16( RING_buffer_t* buf, uint16_t symbol);
uint8_t RING_Pop(RING_buffer_t *buf);
uint16_t RING_Pop16(RING_buffer_t *buf);
int32_t RING_ShowSymbol(const RING_buffer_t *buf, uint16_t symbolNumber);

#ifdef __cplusplus
}
#endif

#endif /* RING_BUFFER_H */
