/**
  ******************************************************************************
  * @file    ...\src\main.h
  * @date    09-04-2019
  * @brief   None
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "MDR1986VE1T.h"

// Application
#include "arm_port.h"
#include "arm_app.h"
#include "systick.h"
#include "mil_std_port.h"
#include "serial_app.h"
#include "mil_std_app.h"
#include "leds.h"

// Drivers
#include "1986VE1T_IT.h"
#include "RST_CLK.h"
#include "UART2.h"

// IP
#include "udp.h"
#include "ethfunc.h"
#include "tcp_ip_structs.h"
#include "ip.h"
#include "arp.h"
#include "ICMP.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions --------------------------------------------------------*/

#endif // __MAIN_H
