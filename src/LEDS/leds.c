/**
	*****************************************************************************
	* @file			...\src\LEDS\leds.c
	* @date			23-04-18
	* @brief		
	*****************************************************************************
	*/

/* Includes -----------------------------------------------------------------*/
#include "leds.h"
	
/* Private typedef ----------------------------------------------------------*/
/* Private define -----------------------------------------------------------*/
/* Private macro ------------------------------------------------------------*/
/* Private variables --------------------------------------------------------*/
/* Private function prototypes ----------------------------------------------*/
/* Private functions --------------------------------------------------------*/
void LEDS_PortInit(void)
{
    MDR_RST_CLK->PER_CLOCK |= CLOCK_EN_LEDS;
    
    LEDS_PORT->FUNC |=  (PORT_FUNC_MODE_PORT << LED0_MODE_Pos)
                       |(PORT_FUNC_MODE_PORT << LED1_MODE_Pos)
                       |(PORT_FUNC_MODE_PORT << LED2_MODE_Pos);
    
    LEDS_PORT->ANALOG |= (LED0_BIT | LED1_BIT | LED2_BIT);
    LEDS_PORT->OE |= (LED0_BIT | LED1_BIT | LED2_BIT);
    
    LEDS_PORT->PWR |=  (PORT_PWR_MAX_FAST << LED0_PWR_Pos)
                      |(PORT_PWR_MAX_FAST << LED1_PWR_Pos)
                      |(PORT_PWR_MAX_FAST << LED2_PWR_Pos);
    
    LED_Off(LED0);
    LED_Off(LED1);
    LED_Off(LED2);
}

///////////////////////////////////////////////////////////////////////////////
void LED_On(leds_num led)
{    
    LEDS_PORT->SETTX = (1 << led);
}

///////////////////////////////////////////////////////////////////////////////
void LED_Off(leds_num led)
{
    LEDS_PORT->CLRTX = (1 << led);
}

/************************ (C) COPYRIGHT *********************END OF FILE******/
