/**
	*****************************************************************************
	* @file			.../src/LEDS/leds.h
	* @date			23-04-18
	* @brief		
	*****************************************************************************
	*/

/* Define to prevent recursive inclusion ------------------------------------*/
#ifndef __LEDS_H
#define __LEDS_H

/* Includes -----------------------------------------------------------------*/
#include "MDR1986VE1T.h"

/* Exported types -----------------------------------------------------------*/
typedef enum
{
    LED0 = 8,
    LED1 = 9,
    LED2 = 10
}leds_num;

/* Exported constants -------------------------------------------------------*/
/* Exported macro -----------------------------------------------------------*/
#define LED0_BIT    (1 << LED0)
#define LED1_BIT    (1 << LED1)
#define LED2_BIT    (1 << LED2)

#define LED0_MODE_Pos   (PORT_FUNC_MODE8_Pos)
#define LED1_MODE_Pos   (PORT_FUNC_MODE9_Pos)
#define LED2_MODE_Pos   (PORT_FUNC_MODE10_Pos)

#define LED0_PWR_Pos    PORT_PWR8_Pos
#define LED1_PWR_Pos    PORT_PWR9_Pos
#define LED2_PWR_Pos    PORT_PWR10_Pos

#define CLOCK_EN_LEDS   RST_CLK_PER_CLOCK_PCLK_EN_PORTD
#define LEDS_PORT       MDR_PORTD


/* Exported functions -------------------------------------------------------*/
void LEDS_PortInit(void);
void LED_On(leds_num led);
void LED_Off(leds_num led);

#endif /* __LEDS_H */

/************************ (C) COPYRIGHT *******END OF FILE********************/
