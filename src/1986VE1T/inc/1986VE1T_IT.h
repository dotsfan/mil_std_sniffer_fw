/**
  ******************************************************************************
  * @file    ...\src\1986VE1T\inc\1986VE1T_IT.h
  * @date    09-04-2019
  * @brief   �������� ��������� ������������ ����������
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __1986VE1T_IT_H
#define __1986VE1T_IT_H
	
/* Includes ------------------------------------------------------------------*/
#include "MDR1986VE1T.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions -------------------------------------------------------*/

//void Reset_Handler(void);
void NMI_Handler(void);

void HardFault_Handler(void);

void SVC_Handler(void);

void PendSV_Handler(void);

void USB_Handler(void);

void DMA_Handler(void);

void BUSY_Handler(void);

void ARINC429R_Handler(void);

void POWER_Handler(void);

void WWDG_Handler(void);

void ADC_Handler(void);

void BKP_Handler(void);

void ETHERNET_Handler(void);

void CAN1_Handler(void);
void CAN2_Handler(void);

//void MIL_STD_1553B2_Handler(void);
//void MIL_STD_1553B1_Handler(void);

//void UART1_Handler(void);
//void UART2_Handler(void);

void SSP1_Handler(void);
void SSP2_Handler(void);
void SSP3_Handler(void);

void TIMER1_Handler(void);
void TIMER2_Handler(void);
void TIMER3_Handler(void);
void TIMER4_Handler(void);

void ARINC429T1_Handler(void);
void ARINC429T2_Handler(void);
void ARINC429T3_Handler(void);
void ARINC429T4_Handler(void);

void EXT_INT1_Handler(void);
void EXT_INT2_Handler(void);
void EXT_INT3_Handler(void);
void EXT_INT4_Handler(void);

#endif // __1986VE1T_H
