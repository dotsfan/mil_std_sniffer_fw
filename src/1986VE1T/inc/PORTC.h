/**
  ******************************************************************************
  * @file    UDP_Application\src\1986VE1T\inc\PORTC.h
  * @date    01-06-2016
  * @brief   �������� ��������� �������� ��� ������ � ������ PORTC
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PORTC_H
#define __PORTC_H

/* Includes ------------------------------------------------------------------*/
#include "MDR1986VE1T.h"

/** @addtogroup UDP_Application
	* @{
	*/

/** @addtogroup PORTC_Application_Driver
	* @brief			������� ���������� ����� PORTC
	* @{
	*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions -------------------------------------------------------*/

/** @defgroup PORTC_Exported_Functions PORTC Exported Functions
	* @brief		��������� ��� ������ � ������ PORTC
  * @{
  */

/**
	* @brief	��������� ����� PORTC ��� ������ � ������ UART1.
	* @param	None
	* @note		None
	* @retval	None 
  */
void PORTC_Config(void);

/** @}	*/ /* End of group PORTC_Exported_Functions */

/** @}	*/ /* End of group PORTC_Application_Driver */

/** @}	*/ /* End of group UDP_Application */

#endif // __PORTC_H
