/**
  ******************************************************************************
  * @file    UDP_Application/src/1986VE1T/inc/PORTB.h
  * @date    01-06-2016
  * @brief   �������� ��������� �������� ��� ������ � ������ PORTB
  ******************************************************************************
  */
	
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PORTB_H
#define __PORTB_H

/* Includes ------------------------------------------------------------------*/
#include "MDR1986VE1T.h"

/** @addtogroup UDP_Application
	* @{
	*/

/** @addtogroup PORTB_Application_Driver
	* @brief			������� ���������� ����� PORTB
	* @{
	*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/** @defgroup PORTB_Exported_Constants PORTB Exported Constants
	* @brief		��������� ��� ������ � ������ PORTB
  * @{
  */
	
#define YelLed	14 /*!< ������ (Link) ��������� */
#define GrLed 	15 /*!< ������� (PHY) ��������� */

/** @} */ /* End of group  PORTB_Exported_Constants*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions -------------------------------------------------------*/

/** @defgroup PORTB_Exported_Functions PORTB Exported Functions
	* @brief		��������� ��� ������ � ������ PORTB
  * @{
  */

/**
	* @brief	��������� ����� PORTB ��� ������ � ������ ������.
	* @param	None
	* @note		None
	* @retval	None 
  */
void PORTB_Config(void);

/** @}	*/ /* End of group PORTB_Exported_Functions */

/** @}	*/ /* End of group PORTB_Application_Driver */

/** @}	*/ /* End of group UDP_Application */

#endif // __PORTB_H
