/**
  ******************************************************************************
  * @file    UDP_Application\src\1986VE1T\inc\UART2.h
  * @date    04-10-2016
  * @brief   �������� ��������� ������� ��� ������ � ������������ UART2
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __UART2_H
#define __UART2_H

/* Includes ------------------------------------------------------------------*/
#include "MDR1986VE1T.h"

/** @addtogroup UDP_Application
	* @{
	*/

/** @defgroup UART_Application_Driver UART Application Driver
	* @brief		������� ���������� ����������� UART2
	* @{
	*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

/* Exported functions --------------------------------------------------------*/

/** @defgroup UART_Exported_Functions UART Exported Functions
	* @brief		������� ��� ������ � ������� UART2
	* @{
	*/

/**
	* @brief  ��������� ��������� ������� UART2
	* @param  None
	* @note		None	
  * @retval None
  */
void UART2_Config(void);

// ��������� ������� UART2
void UART2_PortInit(void);


/** @} */ /* End of group UART_Exported_Functions */

/** @} */ /* End of group UART_Application_Driver */

/** @} */ /* End of group UDP_Application */

#endif // __UART2_H
