/**
  ******************************************************************************
  * @file    .../src/1986VE1T/src/PORTD.c 
  * @date    04-10-2016
  * @brief   Содержит реализацию драйвера для работы с PORTD
  ******************************************************************************
  */
	
/* Includes ------------------------------------------------------------------*/
#include "PORTD.h"

/** @addtogroup UDP_Application
	* @{
	*/
	
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
void PORTD_Config(void)
{
	// Вкл. тактирование PORTD
	MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_PORTD;
	
	// Настройка порта для работы с UART2
	// PD13 (TX), PD14 (RX)
	MDR_PORTD->FUNC |= (PORT_FUNC_MODE_MAIN << PORT_FUNC_MODE13_Pos ) | 
					   (PORT_FUNC_MODE_MAIN << PORT_FUNC_MODE14_Pos );
	
	// Цифровой режим
	MDR_PORTD->ANALOG |= (1 << 13) | (1 << 14);	
	
	MDR_PORTD->PWR |= (PORT_PWR_MAX_FAST << PORT_PWR13_Pos) | 
					  (PORT_PWR_MAX_FAST << PORT_PWR14_Pos);
}

/** @} */ /* End of group UDP_Application */
