/**
  ******************************************************************************
  * @file    ...\src\1986VE1T\src\UART1.c
  * @date    09-04-2019
  * @brief   Содержит реализацию модуля для работы с контроллером UART1
             Сопряжение с МНП
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "UART1.h"

/* Private define ------------------------------------------------------------*/
//#define MNP_WR_ENABLE
/* Private macro -------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
void UART1_Config(void)
{
	// Разрешаем тактирование UART1
	MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_UART1;
	
	// Вкл. тактирование модуля. Уст. частоту HCLK/4 (128/4 = 32 MHz)
	MDR_RST_CLK->UART_CLOCK |= (1 << RST_CLK_UART_CLOCK_UART1_CLK_EN_Pos) |
														(RST_CLK_UART_CLOCK_UART_BRG_HCLK_DIV_4  );	
		

	// Установка коэффициентов для частоты UART (F_uart) = 32MHz, baudrate = 115200kbps
	// IBRD = int(F_uart / (16*baudrate)). 
	// fract(IBRD) = (F_uart / (16*baudrate)) - IBRD.
	// FBRD = int(64*fract(IBRD) + 0.5)
	MDR_UART1->IBRD = 0x0011;	//16(0x10) для 30МГц, 17(0x11) для 32МГц. Целая часть делителя скорости обмена данными
	MDR_UART1->FBRD = 0x0018;	//18(0x12) для 30МГц, 24(0x18) для 32МГц. Дробная часть делителя скорости обмена данными
		
	// Буфер FIFO выключен. Структура кадра 8N1
	MDR_UART1->LCR_H = (UART_LCR_H_WLEN_8_BITS << UART_LCR_H_WLEN_Pos) | UART_LCR_H_STP2;
	
	// Разрешаем прерывания по приему символа
	MDR_UART1->IMSC |= UART_IT_RX;
	
#ifdef MNP_WR_ENABLE
	MDR_UART1->IMSC |= UART_IT_TX;
#endif

	// Разрешаем прием
	MDR_UART1->CR |= UART_CR_RXE;
	
//#ifdef MNP_WR_ENABLE
	MDR_UART1->CR |= UART_CR_TXE;
//#endif

	// Вкл. UART
	MDR_UART1->CR |= UART_CR_UARTEN;
	
	// Разрешаем глобальные прерывания	
	NVIC_EnableIRQ(UART1_IRQn);
}

//////////////////////////////////////////////////////////////////////////////
void UART1_PreConfig(void)
{
	// Разрешаем тактирование UART1
	MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_UART1;
	
	// Вкл. тактирование модуля. Уст. частоту HCLK/4 (128/4 = 32 MHz)
	MDR_RST_CLK->UART_CLOCK |= (1 << RST_CLK_UART_CLOCK_UART1_CLK_EN_Pos) |
														(RST_CLK_UART_CLOCK_UART_BRG_HCLK_DIV_4  );	
		

	// Установка коэффициентов для частоты UART (F_uart) = 32MHz, baudrate = 9600
	// IBRD = int(F_uart / (16*baudrate)). 
	// fract(IBRD) = (F_uart / (16*baudrate)) - IBRD.
	// FBRD = int(64*fract(IBRD) + 0.5)
	MDR_UART1->IBRD = 0x00D0;	//208(0x00D0) для 32МГц. Целая часть делителя скорости обмена данными
	MDR_UART1->FBRD = 0x0015;	//21(0x0015) для 32МГц. Дробная часть делителя скорости обмена данными
		
	// Буфер FIFO выключен. Структура кадра 8N1
	MDR_UART1->LCR_H = (UART_LCR_H_WLEN_8_BITS << UART_LCR_H_WLEN_Pos) |
											(UART_LCR_H_STP2);

	// Разрешаем прием и передачу
	MDR_UART1->CR |= (UART_CR_TXE | UART_CR_RXE);
	
	// Вкл. UART
	MDR_UART1->CR |= UART_CR_UARTEN;	
}
