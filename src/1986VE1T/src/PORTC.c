/**
  ******************************************************************************
  * @file    .../src/1986VE1T/src/PORTC.c 
  * @date    01-06-2016
  * @brief   Содержит реализацию драйвера для работы с PORTC
  ******************************************************************************
  */
	
/* Includes ------------------------------------------------------------------*/
#include "PORTC.h"

/** @addtogroup UDP_Application
	* @{
	*/
	
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
void PORTC_Config(void)
{
	// Вкл. тактирование PORTC
	MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_PORTC;
	
	// Настройка порта для работы с UART1
	// Настройка пинов PC3, PC4 в режиме основной функции	
	MDR_PORTC->FUNC = (PORT_FUNC_MODE_MAIN << PORT_FUNC_MODE3_Pos ) | 
					  (PORT_FUNC_MODE_MAIN << PORT_FUNC_MODE4_Pos );
	
	MDR_PORTC->RXTX = 0x0000;
	
	// Цифровой режим работы
	MDR_PORTC->ANALOG = (1 << 3) | (1 << 4);	
	MDR_PORTC->PWR = (PORT_PWR_MAX_FAST << PORT_PWR3_Pos) | 
					 (PORT_PWR_MAX_FAST << PORT_PWR4_Pos);
}

/** @} */ /* End of group UDP_Application */
