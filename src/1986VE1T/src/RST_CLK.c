/**
  *****************************************************************************
  * @file    ...\src\1986VE1T\src\RST_CLK.c 
  * @date    09-04-2019
  * @brief   Содержит реализацию модуля для работы с контроллером тактовой частоты
  *****************************************************************************
  */

/* Includes -----------------------------------------------------------------*/
#include "RST_CLK.h"

/* Private define -----------------------------------------------------------*/
/* Private macro ------------------------------------------------------------*/
/* Private typedef ----------------------------------------------------------*/
/* Private variables --------------------------------------------------------*/
/* Private functions --------------------------------------------------------*/
void RST_CLK_Config(void)
{
	MDR_RST_CLK->PER_CLOCK = RST_CLK_PER_CLOCK_PCLK_EN_RST_CLK;

	MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_BKP;
	
	MDR_BKP->REG_0E &= ~(BKP_REG_0E_LOW_Msk | BKP_REG_0E_SELECTRI_Msk);
	MDR_BKP->REG_0E |= (7 << BKP_REG_0E_LOW_Pos) | (7 << BKP_REG_0E_SELECTRI_Pos);

	//HSE On, Oscillator mode; HSE2 On, Oscillator mode
	MDR_RST_CLK->HS_CONTROL = (1 << RST_CLK_HS_CONTROL_HSE_ON_Pos)  |
														(0 << RST_CLK_HS_CONTROL_HSE_BYP_Pos) |
														(1 << RST_CLK_HS_CONTROL_HSE_ON2_Pos) |
														(0 << RST_CLK_HS_CONTROL_HSE_BYP2_Pos);

	// Wait until HSE and HSE2 not ready
	while((MDR_RST_CLK->CLOCK_STATUS & RST_CLK_CLOCK_STATUS_HSE_ALL_RDY) == 0){};

	MDR_RST_CLK->CPU_CLOCK =
		(RST_CLK_CPU_CLOCK_CPU_C1_SEL_HSE << RST_CLK_CPU_CLOCK_CPU_C1_SEL_Pos) |
		(RST_CLK_CPU_CLOCK_CPU_C2_SEL_CPU_C1 << RST_CLK_CPU_CLOCK_CPU_C2_SEL_Pos) |
		(RST_CLK_CPU_CLOCK_CPU_C3_SEL_CPU_C2 << RST_CLK_CPU_CLOCK_CPU_C3_SEL_Pos) |
		(RST_CLK_CPU_CLOCK_HCLK_SEL_HSI << RST_CLK_CPU_CLOCK_HCLK_SEL_Pos);
		
	MDR_RST_CLK->PLL_CONTROL =
		(0 << RST_CLK_PLL_CONTROL_PLL_USB_ON_Pos)  |
		(0 << RST_CLK_PLL_CONTROL_PLL_USB_RLD_Pos) |
		(0 << RST_CLK_PLL_CONTROL_PLL_CPU_ON_Pos)  |
		(0 << RST_CLK_PLL_CONTROL_PLL_CPU_PLD_Pos) |
		(0 << RST_CLK_PLL_CONTROL_PLL_USB_MUL_Pos) |
		(7 << RST_CLK_PLL_CONTROL_PLL_CPU_MUL_Pos);
	
	SET_BIT(MDR_RST_CLK->PLL_CONTROL, RST_CLK_PLL_CONTROL_PLL_CPU_ON_Pos);
	
	SET_BIT(MDR_RST_CLK->PLL_CONTROL, RST_CLK_PLL_CONTROL_PLL_CPU_PLD_Pos);
	CLR_BIT(MDR_RST_CLK->PLL_CONTROL, RST_CLK_PLL_CONTROL_PLL_CPU_PLD_Pos);
	
	while ((MDR_RST_CLK->CLOCK_STATUS & RST_CLK_CLOCK_STATUS_PLL_CPU_RDY) == 0){};

	// EEPROM_CNTRL Clock enable	
	MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_EEPROM_CNTRL;
	MDR_EEPROM->CMD = (3 << EEPROM_CMD_DELAY_Pos);
	MDR_RST_CLK->PER_CLOCK &= ~RST_CLK_PER_CLOCK_PCLK_EN_EEPROM_CNTRL;

		// Выбираем для CPU_C1 - HSE; CPU_C2 - PLLCPU0;
	// CPU_C3 - CPU_C2; HCLK - CPU_C3
	MDR_RST_CLK->CPU_CLOCK =
		(RST_CLK_CPU_CLOCK_CPU_C1_SEL_HSE << RST_CLK_CPU_CLOCK_CPU_C1_SEL_Pos) |
		(RST_CLK_CPU_CLOCK_CPU_C2_SEL_PLL_CPU << RST_CLK_CPU_CLOCK_CPU_C2_SEL_Pos) |
		(RST_CLK_CPU_CLOCK_CPU_C3_SEL_CPU_C2 << RST_CLK_CPU_CLOCK_CPU_C3_SEL_Pos) |
		(RST_CLK_CPU_CLOCK_HCLK_SEL_CPU_C3 << RST_CLK_CPU_CLOCK_HCLK_SEL_Pos);
}
