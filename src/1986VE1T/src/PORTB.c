/**
  ******************************************************************************
  * @file    .../src/1986VE1T/src/PORTB.c 
  * @date    01-06-2016
  * @brief   Содержит реализацию драйвера для работы с PORTB
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "PORTB.h"

/** @addtogroup UDP_Application
	* @{
	*/
	
/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
void PORTB_Config(void)
{
	// Вкл. тактирование PORTB
	MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_PORTB;
	
	// Цифровой режим работы выводов порта
	MDR_PORTB->ANALOG |= ((1<<YelLed) | (1<<GrLed));

	// Режим работы - порт
	MDR_PORTB->FUNC &= ~((PORT_FUNC_MODE_Msk << YelLed*2) | 
						 (PORT_FUNC_MODE_Msk << GrLed*2));
	
	// Направление порта - выход
	MDR_PORTB->OE |= ((1<<YelLed) | (1<<GrLed));

	// Подтяжка порта - без подтяжки
	MDR_PORTB->PULL &= ~((1<<YelLed) | (1<<(YelLed+PORT_PULL_UP_Pos))|
						 (1<<GrLed) | (1U<<(GrLed+PORT_PULL_UP_Pos)));
	
	// Режим работы выходного драйвера - управляемый драйвер
	MDR_PORTB->PD &= ~((1<<YelLed) | (1<<GrLed));
	
	// Режим мощности передатчика - максимально быстрый фронт
	MDR_PORTB->PWR |= ((PORT_PWR_Msk<<YelLed*2) | (PORT_PWR_Msk<<GrLed*2));
	
	// Установка нулей
	MDR_PORTB->RXTX &= ~((1<<YelLed) | (1<<GrLed));
}

/** @} */ /* End of group UDP_Application */
