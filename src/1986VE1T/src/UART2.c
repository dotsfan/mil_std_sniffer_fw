/**
  ******************************************************************************
  * @file    ...\src\1986VE1T\src\UART2.c
  * @date    09-04-2019
  * @brief   Содержит реализацию модуля для работы с UART2
             Сопряжение с внешними устройствами
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "UART2.h"

/* Private define ------------------------------------------------------------*/
#define RATE_9600				0x50
#define RATE_19200			0x40
#define RATE_38400			0x30
#define RATE_56000			0x20
#define RATE_57600			0x10
#define RATE_115200			0x00
#define RATE_128000			0x80

#define HCLK_CLOCK			128000000

/* Private macro -------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
void UART2_Config(void)
{
	uint32_t ibdr, fbdr;
	uint8_t br = RATE_115200;
	
	// Разрешаем тактирование UART2
	MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_UART2;
	
	// Вкл. тактирование модуля. Уст. частоту HCLK/4 (128/4 = 32 MHz)
	MDR_RST_CLK->UART_CLOCK |= (1 << RST_CLK_UART_CLOCK_UART2_CLK_EN_Pos) |
														 (RST_CLK_UART_CLOCK_UART_BRG_HCLK_DIV_4  );	
		

	MDR_UART2->CR &= ~UART_CR_UARTEN;
	
	// Установка коэф. для частоты UART (F_uart) = 32MHz, baudrate = 115200kbps
	// IBRD = int(F_uart / (16*baudrate)) = int(IBRD). 
	// fract(IBRD) = (F_uart / (16*baudrate)) - int(IBRD).
	// FBRD = int(64*fract(IBRD) + 0.5)
//	MDR_UART2->IBRD = 0x0011;	// 0x0011
//	MDR_UART2->FBRD = 0x0018;	// 0x0018
	switch (br)
	{
		case RATE_9600:
			ibdr = HCLK_CLOCK / 9600 / 16;
			fbdr = ((float)HCLK_CLOCK / 9600.0 / 16.0 - HCLK_CLOCK / 9600 / 16) * 64;
			break;
		case RATE_19200:
			ibdr = HCLK_CLOCK / 19200 / 16;
			fbdr = ((float)HCLK_CLOCK / 19200.0 / 16.0 - HCLK_CLOCK / 19200 / 16) * 64;
			break;
		case RATE_38400:
			ibdr = HCLK_CLOCK / 38400 / 16;
			fbdr = ((float)HCLK_CLOCK / 38400.0 / 16.0 - HCLK_CLOCK / 38400 / 16) * 64;
			break;
		case RATE_56000:
			ibdr = HCLK_CLOCK / 56000 / 16;
			fbdr = ((float)HCLK_CLOCK / 56000.0 / 16.0 - HCLK_CLOCK / 56000 / 16) * 64;
			break;
		case RATE_57600:
			ibdr = HCLK_CLOCK / 57600 / 16;
			fbdr = ((float)HCLK_CLOCK / 57600.0 / 16.0 - HCLK_CLOCK / 57600 / 16) * 64;
			break;
		case RATE_115200:
			ibdr = HCLK_CLOCK / 115200 / 16;
			fbdr = ((float)HCLK_CLOCK / 115200.0 / 16.0 - HCLK_CLOCK / 115200 / 16) * 64;
			break;
		default:
			ibdr = HCLK_CLOCK / 128000 / 16;
			fbdr = ((float)HCLK_CLOCK / 128000.0 / 16.0 - HCLK_CLOCK / 128000 / 16) * 64;
	}
	MDR_UART2->IBRD = ibdr;
	MDR_UART2->FBRD = fbdr;
		
	// 8 бит данных + бит четности, fifo disable
	MDR_UART2->LCR_H = (UART_LCR_H_WLEN_8_BITS << UART_LCR_H_WLEN_Pos) |
										  UART_LCR_H_PEN | UART_LCR_H_EPS;
										
	// Очищаем флаги разрешенных прерываний
	MDR_UART2->ICR = 0;
	
	// Разрешаем прерывания по приему
	MDR_UART2->IMSC = UART_IMSC_RXIM;
	
	// Разрешаем прием и передачу
	MDR_UART2->CR |= (UART_CR_RXE | UART_CR_TXE);
	
	// Вкл. UART
	MDR_UART2->CR |= UART_CR_UARTEN;

	// Разрешаем глобальные прерывания
	NVIC_EnableIRQ(UART2_IRQn);
}

///////////////////////////////////////////////////////////////////////////////
void UART2_PortInit(void)
{
	// Вкл. тактирование порта
	MDR_RST_CLK->PER_CLOCK |= RST_CLK_PER_CLOCK_PCLK_EN_PORTD;
	
	// Настройка пинов порта для работы с UART2
	MDR_PORTD->FUNC |= ((PORT_FUNC_MODE_MAIN << PORT_FUNC_MODE13_Pos) |
										  (PORT_FUNC_MODE_MAIN << PORT_FUNC_MODE14_Pos));
	
	// Цифровой режим работы
	MDR_PORTD->ANALOG |= ((1 << 13) | (1 << 14));
	
	// Скорость изменения фронта - максимально быстрый
	MDR_PORTD->PWR |= ((PORT_PWR_MAX_FAST << PORT_PWR13_Pos) |
										 (PORT_PWR_MAX_FAST << PORT_PWR14_Pos));
	
}
